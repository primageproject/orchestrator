#!/bin/bash -l
#SBATCH -N 1
#SBATCH --ntasks-per-node=1
#SBATCH --time=00:20:00
#SBATCH -A plgprimage4-cpu
#SBATCH -p plgrid-now

# Finish with error on first command with error
set -e

mkdir $SCRATCH/orchestrator-$SLURM_JOB_ID
cd $SCRATCH/orchestrator-$SLURM_JOB_ID

## Copy initialization file to scratch
cp $PLG_GROUPS_STORAGE/plggprimage/vph-hfv3-4.0/initial_input/initialization.json $SCRATCH/orchestrator-$SLURM_JOB_ID/

module use /net/people/plgrid/plgnikolow/modulefiles
module load oneclient

# Values of these 3 variables should be converted to variables
export INPUTS=PRIMAGE_DATALAKE/carlosbaeza/01_Neuroblastoma_45_Neuroblastoma/RM411RMABDOMEN20161207/biomarker/test_orchestrator/
export OUTPUTS=PRIMAGE_HOME/test/outputs
export ONECLIENT_ACCESS_TOKEN=
export ONECLIENT_PROVIDER_HOST=plg-cyfronet-01.datahub.egi.eu
export DB_PASSWORD=
export PROXY=

mkdir onedata
oneclient --force-proxy-io onedata

mkdir inputs
mkdir outputs

cp onedata/$INPUTS/analysis.json inputs

## Run the orchestrator
module load python/3.8.2-gcccore-9.3.0
pip3 install --upgrade pip
pip3 install pandas
pip3 install mysql-connector-python
python3 $PLG_GROUPS_STORAGE/plggprimage/vph-hfv3-4.0/src/master.py

## Store the outputs in the onedata
mkdir -p onedata/$OUTPUTS
cp -r outputs/* onedata/$OUTPUTS

## Cleanup
oneclient -u onedata
#rm -rf study outputs
#rmdir onedata

