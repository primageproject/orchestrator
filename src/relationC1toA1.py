import os
import time
import pandas as pd
import pythonSQL as pysql


class C1toA1:

    def __init__(self, dbname, da, al, name, idnextmodel, cluster, iofolder, poll_time, pwd):

        ## Model name (for prints)
        self.name = "Relation Cellular to ABM"
        
        ## Cluster name
        self.cluster = cluster

        ## Creates a instance of SQLpython class with a new database connection 
        self.SQL = pysql.SQLpython(cluster, pwd, False, dbname)

        ## constant column name
        self.COLUMN_NAME = 'value'
        self.COLUMN_BINS = 'eq_spaced'
 
        ## Stopping criterion 
        self.finished = False
        ## Time variables
        self.total_process = 0
        self.DB_time = 0
        self.polling_process = 0

        ## Set the interval in seconds to pool
        self.poll_time = poll_time
        ## Set next model id
        self.nextmodel = idnextmodel
        ## Set the name of I/O folder
        self.iofolder = iofolder

        ## table name of input polled
        self.input_table = 'input_'+da
        ## table name to poll
        self.poll_table = 'output_'+da
        ## table name to write inputs
        self.write_table = 'input_'+al  

        ## List of columns used
        self.columnsRead = ["i_volume", "i_oxygen", "i_cellularity_alive_NB", "i_cellularity_alive_SC", "i_cellularity_apoptotic_NB", 
        "i_cellularity_apoptotic_SC", "i_cellularity_necrotic_NB", "i_cellularity_necrotic_SC", "i_cells_count"]
        self.columnsWrite = ["volume", "cellularity_alive_NB", "cellularity_alive_SC", "cellularity_apoptotic_NB", "cellularity_apoptotic_SC", 
        "cellularity_necrotic_NB", "cellularity_necrotic_SC", "cells_count", "mean_telomer_NB", "std_telomer_NB", "mean_telomer_SC", "std_telomer_SC", 
        "mean_apoptotic_NB", "std_apoptotic_NB", "mean_necrotic_NB", "std_necrotic_NB",  "mean_apoptotic_SC", "std_apoptotic_SC",
        "mean_necrotic_SC", "std_necrotic_SC", "mean_diff_NB", "std_diff_NB", "oxygen", "start", "end", "effects"]

        ## Verify if there are interruptions
        query = f"""SELECT provenance_sim FROM {self.SQL.mydb.database}.{self.input_table} WHERE id{self.input_table} IN 
        (SELECT id_{self.input_table} FROM {self.SQL.mydb.database}.{self.poll_table});"""
        provsims1 = self.SQL.mysql_execute(query)
        provsims = []
        for p in provsims1:
            if p[0] != None:
                provsims.append(int(p[0]))
        query = f"SELECT provenance_sim, count(*) FROM {self.SQL.mydb.database}.{self.write_table} GROUP BY provenance_sim;"
        provsims3 = self.SQL.mysql_execute(query)
        provsimsconcluded = []
        for p in provsims3:
            if p != None:
                provsim = int(p[0])
                ninputsABM = int(p[1])
                ## Check if all inputsA1 were inserted
                query = f"SELECT nbins+1 FROM {self.SQL.mydb.database}.provenance_sim WHERE idprov = {provsim};"
                ngoal = self.SQL.mysql_execute(query)[0][0]
                if ngoal == ninputsABM:
                    provsimsconcluded.append(provsim)                          
        provsiminterrupteds = list(set(provsims) - set(provsimsconcluded))
        provsstring = self.SQL.listtostring(provsiminterrupteds,True,False)
        if provsiminterrupteds != []:
            query = f"""SELECT id{self.poll_table} FROM {self.SQL.mydb.database}.{self.poll_table} WHERE id_{self.input_table} IN 
            (SELECT id{self.input_table} FROM {self.SQL.mydb.database}.{self.input_table} WHERE provenance_sim IN ({provsstring}));"""
            interrupteds = self.SQL.mysql_execute(query)
        else:
            interrupteds = []

        ## For each interrupted, write on exec_table
        for interrupted in interrupteds:
            query = f"SELECT id{self.poll_table} FROM {self.SQL.mydb.database}.{self.poll_table} WHERE id{self.poll_table} = {str(interrupted[0])};"
            id = self.SQL.mysql_execute(query)[0][0]
            columns = f"`table_name`, `datetime`, `provenance_id`, `action`"
            values = f"\"{self.poll_table}\", NOW(), {id}, 'insert'"
            self.SQL.mysql_insert("exec_table", columns, values)


    def wait(self):

        while self.finished == False: 

            ## process time
            process_start_p = time.time()
            
            results = self.SQL.mysql_execute(f"SELECT * FROM {self.SQL.mydb.database}.exec_table WHERE `table_name` = \"{self.poll_table}\" AND action = \"insert\";")

            ## Stop the stopwatch / counter                  
            process_stop_p = time.time()                     
            ## Sum the time of the module
            self.polling_process += process_stop_p - process_start_p 


            if results != []:
  
                ## process time
                process_start = time.time()
                
                ## Gets the new insertion on output cell model table
                for i in range(len(results)): 

                    process_db_start = time.time()

                    ## Get data
                    idoutput = str(results[i][3])
                    query = f"SELECT idsimulation, id_orchestration FROM {self.SQL.mydb.database}.simulation WHERE idsimulation = (SELECT id_simulation FROM {self.poll_table} WHERE id{self.poll_table} = "+idoutput+");"
                    result = self.SQL.mysql_execute(query)
                    idsim = str(result[0][0])
                    idorch = str(result[0][1])
                    
                    ## Get loop
                    query = f'SELECT current_simtime FROM {self.SQL.mydb.database}.orchestration WHERE idorchestration = {idorch};'
                    simtime = self.SQL.mysql_execute(query)[0][0]

                    ## Get output data
                    query = f'SELECT names_list, path, id_{self.input_table} FROM {self.SQL.mydb.database}.{self.poll_table} WHERE id{self.poll_table} = {str(idoutput)};'
                    result = self.SQL.mysql_execute(query)[0]
                    names_list = result[0].split(",")
                    path = result[1]
                    idinput = str(result[2])

                    ## get sample of elements
                    query = f'SELECT id_elements FROM {self.SQL.mydb.database}.{self.input_table} WHERE id{self.input_table} = {str(idinput)};'
                    sample = self.SQL.mysql_execute(query)[0][0].split()
                    
                    print("Preparing ABM data")

                    query = f'SELECT id_simulation from {self.SQL.mydb.database}.{self.poll_table} where id{self.poll_table} = {idoutput} LIMIT 1'
                    idsim = self.SQL.mysql_execute(query)[0][0]
                    query = f'SELECT provenance_sim FROM {self.SQL.mydb.database}.{self.input_table} WHERE id{self.input_table} = {idinput};'
                    sim_prov = self.SQL.mysql_execute(query)[0][0]
                    query = f'SELECT `loop`, nelems FROM {self.SQL.mydb.database}.simulation WHERE idsimulation = {idsim}'
                    result = self.SQL.mysql_execute(query)
                    loop = result[0][0]
                    nelems = result[0][1]

                    
                    ## Create a association between the input ABM to the relation table
                    self.addrelationtable(sim_prov)
                        
                    process_db_stop = time.time()
                    ## Update db time
                    self.DB_time += process_db_stop - process_db_start

                    pathloop = path+"../"
                    if simtime == 0: ## if is first loop, initialize fake files
                        telo = open(f"{pathloop}telomer_length.txt",'w')
                        death = open(f"{pathloop}death_signals.txt",'w')
                        diff = open(f"{pathloop}differenciation_NB.txt",'w')
                        for n in range(nelems):
                            telo.write("0 0 0 0\n") 
                            death.write("0 0 0 0 0 0 0 0\n") 
                            diff.write("0 0\n") 
                        telo.close()
                        death.close()
                        diff.close()
                        
                    ## Get variables from files
                    paths = [path, path, path, pathloop, pathloop, pathloop]
                    names = [names_list[0], names_list[1], names_list[2], "telomer_length.txt", "death_signals.txt", "differenciation_NB.txt"]
                    headers = [0, 0, 0, None, None, None] ## If header, number of the row of the header
                    ## Read outp files
                    [start, end, effects, telo, death, diff] = self.load_variable(paths, names, headers)
                    
                    ## Create subfolder
                    newpath = self.createFolder(path, sim_prov)

                    ## Prepare all input files
                    for i in range(len(sample)):

                        elindex = int(sample[i])-1 ## index of whole mesh values
                        indexparticularized = i ## index of particularized values
                        
                        process_db_start = time.time()

                        ## Get id of the relation_table
                        query = f'SELECT idrelation_element FROM {self.SQL.mydb.database}.relation_element_{sim_prov} WHERE element_number = {str(sample[i])} AND provenance_sim = {str(sim_prov)};'
                        idrelation = self.SQL.mysql_execute(query)[0][0]

                        ## Check if input for idrelation was not inserted (for cases of interrupted simulations)
                        query = f'SELECT id{self.write_table} FROM {self.SQL.mydb.database}.{self.write_table} WHERE path = \"{newpath}\" AND id_element = {str(idrelation)}'
                        exist = self.SQL.mysql_execute(query)

                        if exist == []: ## If ABM input was not inserted, insert it
                            ## create simulation for USFD model
                            desc = self.write_table+" simulation from particularization"
                            idsim = self.createSimulation(desc, self.nextmodel, idorch, loop, nelems)
                            ## get all the values to insert new inputs
                            VOI = [] ## Values Of Interest
                            [VOI] = self.getValuesOfSampleElement(idrelation, sim_prov)
                            process_db_stop = time.time()
                            ## Update db time
                            self.DB_time += process_db_stop - process_db_start
                            v_tumour = VOI[0]
                            o2 = VOI[1]
                            cells = VOI[2:8]
                            cellscount = VOI[8]
                            etelomer = telo.at[elindex,'value'].split(" ") # elemental telomer_length
                            edeath = death.at[elindex,'value'].split(" ") # elemental death_signals
                            ediff = diff.at[elindex,'value'].split(" ") ## elemental differentiation_NB]
                            eeffect = effects.loc[indexparticularized].to_string() ## elemental effects array
                            starteffect = 0
                            endeffect = 0
                            if start.size > 0:
                                starteffect = start.loc[:,"start_effects"].to_string(index=False)
                                endeffect = end.loc[:,"end_effects"].to_string(index=False)
                            ## set and execute query to insert the input record for usfd model
                            query = (f'INSERT INTO {self.SQL.mydb.database}.{self.write_table} (path,datetime,provenance_sim,id_simulation,'
                            f'id_element, {self.SQL.listtostring(self.columnsWrite, comma = True)})'
                            f' VALUES (\'{str(newpath)}\', NOW(),{str(sim_prov)},{str(idsim)}, {str(idrelation)}, {str(round(v_tumour,6))},'
                            f'{str(round(cells[0],6))},{str(round(cells[1],6))},{str(round(cells[2],6))},{str(round(cells[3],6))},{str(round(cells[4],6))},'
                            f'{str(round(cells[5],6))}, {str(cellscount)}, \'{etelomer[0]}\', \'{etelomer[1]}\', \'{etelomer[2]}\', \'{etelomer[3]}\','
                            f'\'{edeath[0]}\', \'{edeath[1]}\', \'{edeath[2]}\', \'{edeath[3]}\', \'{edeath[4]}\', \'{edeath[5]}\', \'{edeath[6]}\', \'{edeath[7]}\','
                            f'\'{ediff[0]}\', \'{ediff[1]}\', {str(round(o2,6))}, \'{starteffect}\', \'{endeffect}\', \'{eeffect}\');')
                            self.SQL.mysql_execute_NOreturn(query)
                            i+=1
                    
                    ## Excludes from exec_table  
                    query = "DELETE FROM "+self.SQL.mydb.database+".exec_table WHERE provenance_id = "+str(idoutput)+" AND `table_name` = \""+self.poll_table+"\";"
                    self.SQL.mysql_execute_NOreturn(query) 

                ## Stop the stopwatch / counter                      
                process_stop = time.time()                    
                ## Sum the time of the module
                self.total_process += process_stop - process_start  

            else:
                time.sleep(self.poll_time)


    ## Add relation_element table
    def addrelationtable(self, idsim):

        ## check if 'id_element' column exist in the write_table
        query = f"SHOW COLUMNS FROM {self.SQL.mydb.database}.{self.write_table} LIKE 'id_element';"
        result = self.SQL.mysql_execute(query)
        
        ## Create provenance_sim table
        if self.cluster != "local":
            pathrel = f'{os.environ["PLG_GROUPS_STORAGE"]}/plggprimage/vph-hfv3-4.0/'
        else:
            pathrel = '../'
        if result == []:
            with open(f'{pathrel}/database/relation_C1toA1.sql', 'r') as file:
                sql = file.read().replace('<db_name>', self.SQL.mydb.database)
                sql = sql.replace("<input_table>", self.write_table)
                sql = sql.replace("<idsim>", str(idsim))
                self.SQL.mysql_execute_NOreturn(sql)
        print("Relation table updated")


    ## generic function to load other values to pandas
    ## and define the name of the column to be referenced
    def load_variable(self, paths, files, headers):

        # load values from files (oxygen, volume)
        column_name = self.COLUMN_NAME
        i = 0
        variables = [None]*len(paths)
        for i in range(len(paths)):
            # load txt file to pandas data frame
            variables[i] = pd.read_csv(paths[i]+files[i], header=headers[i])
            if headers[i] == None:
                # rename column
                variables[i].columns = [column_name]
            i+=1

        return variables

    
    def createSimulation(self, desc, idModel, idOrch, loop, nelems):
        
        ## Creates simulation
        query = f'INSERT INTO {self.SQL.mydb.database}.`simulation` (`description`, `datetime`, `runtime`, `status`, `id_model`,`id_orchestration`, `loop`, nelems) VALUES (\'{str(desc)}\',NOW(),0,\'started\',{str(idModel)},{str(idOrch)}, {str(loop)}, {str(nelems)});'
        self.SQL.mysql_execute_NOreturn(query)

        ## Gets id new simulation
        query = f"SELECT MAX(idsimulation) FROM {self.SQL.mydb.database}.simulation WHERE `id_orchestration` = {str(idOrch)};"
        idsim = self.SQL.mysql_execute(query)[0][0]

        return idsim

    
    ## Create subfolder of next model (ABM)
    def createFolder(self, path, sim_prov):

        ## Create subdir to the simulation
        dirs = path.split('/')
        ## Removes the last to items from the dirs var
        del(dirs[-1])
        del(dirs[-1])

        ## Create a new dir
        newpath = ""
        for i in range(len(dirs)):
            newpath += dirs[i]+"/"
        newpath = newpath+"abm_"+str(sim_prov)+"/"
        if os.path.isdir(newpath) == False:
            try:
                os.mkdir(newpath)
            except:
                print ("Creation of the directory "+str(newpath)+" failed. Verify if this name file already has been used.")
                exit(1)
            else:
                print ("Successfully created the directory "+str(newpath)) 

        return newpath 

    
    def getValuesOfSampleElement(self, idrelation, prov_sim):
        VOI = []
        query = (f"SELECT {self.SQL.listtostring(self.columnsRead, comma = True)} FROM {self.SQL.mydb.database}.relation_element_{prov_sim}"
        f" WHERE idrelation_element = {str(idrelation)};")
        VOI = self.SQL.mysql_execute(query)
        return VOI  