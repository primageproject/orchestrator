import os
import csv
import sys
import time
import pythonSQL as pysql

class M1toO1:

    def __init__(self, dbname, da, al, idnextmodel, cluster, iofolder, modelspath, poll_time, pwd):

        ## Model name (for prints)
        self.name = "Remeshing"
        
        ## Cluster name
        self.cluster = cluster

        ## set prometheus boolean
        if self.cluster == 'ares':
            self.proxy = os.environ["PROXY"]

        ## Creates a instance of SQLpython class with a new database connection 
        self.SQL = pysql.SQLpython(cluster, pwd, False, dbname)

        ## constant column name
        self.COLUMN_NAME = 'value'
        self.COLUMN_BINS = 'eq_spaced'
 
        ## Stopping criterion 
        self.finished = False
        ## Time variables
        self.total_process = 0
        self.DB_time = 0
        self.polling_process = 0

        ## Set the interval in seconds to pool
        self.poll_time = poll_time
        ## Set next model id
        self.nextmodel = idnextmodel
        ## Set the name of I/O folder
        self.iofolder = iofolder

        ## Models path
        self.modelspath = modelspath

        ## List of files used
        self.nameslist = ["geometry.stl", "volume_f.txt", "volume_i.txt", "updated_O2.txt", "updated_cellularity.txt", "mesh_nodes.inp", "mesh_elements.inp",
         "ratio_VEGF-NB_SC.txt", "telomer_length.txt", "death_signals.txt", "differenciation_NB.txt", "i_vascularization.txt"]

        ## table name of input polled
        self.input_table = 'input_'+da
        ## table name to poll
        self.poll_table = 'output_'+da
        ## table name to write inputs
        self.write_table = 'input_'+al  

        ## Verify if there are interruptions
        query = f"SELECT provenance_sim FROM {self.SQL.mydb.database}.{self.write_table};"
        provsims = self.SQL.mysql_execute(query)
        if provsims != []: ## IFELSE do avoid unknown logical values in SQL
            provsimsstring = ""
            for p in provsims:
                if p[0] != None:
                    provsimsstring+=str(p[0])+","
            provsimsstring = provsimsstring[0:-1] ## Remove last comma
            query = f"""SELECT id{self.poll_table}, id_simulation FROM {self.SQL.mydb.database}.{self.poll_table} WHERE id_simulation NOT IN 
            ({provsimsstring});"""
        else:                   
            query = f'SELECT id{self.poll_table}, id_simulation FROM {self.SQL.mydb.database}.{self.poll_table} WHERE id_simulation;'
        results = self.SQL.mysql_execute(query)
        idsouts = []
        idsims = ""
        for r in results:
            if r[0] != None:
                idsouts.append(int(r[0]))
                idsims+=str(r[1])+","
        idsims = idsims[0:-1]## Remove last comma
        
        interrupteds = []
        if idsims != "":
            query = f"""SELECT `status` FROM {self.SQL.mydb.database}.orchestration WHERE idorchestration IN (SELECT id_orchestration
            FROM {self.SQL.mydb.database}.simulation WHERE idsimulation IN ({idsims}));"""
            statuss = self.SQL.mysql_execute(query)            
            for i in range(len(statuss)):
                if statuss[i][0] == "Running": ## if orchestration did not finish
                    interrupteds.append(idsouts[i])

        ## For each interrupted, write on exec_table
        for interrupted in interrupteds:
            query = f"SELECT id{self.poll_table} FROM {self.SQL.mydb.database}.{self.poll_table} WHERE id{self.poll_table} = {str(interrupted)};"
            id = self.SQL.mysql_execute(query)[0][0]
            columns = f"`table_name`, `datetime`, `provenance_id`, `action`"
            values = f"\"{self.poll_table}\", NOW(), {id}, 'insert'"
            self.SQL.mysql_insert("exec_table", columns, values)

    def wait(self):

        while self.finished == False: 
            
            ## process time
            process_start_p = time.time()
              
            results = self.SQL.mysql_execute(f"SELECT * FROM {self.SQL.mydb.database}.exec_table WHERE `table_name` = \"{self.poll_table}\" AND action = \"insert\";")

            ## Stop the stopwatch / counter                     
            process_stop_p = time.time()                     
            ## Sum the time of the module
            self.polling_process += process_stop_p - process_start_p 

            if results != []:
                  
                ## process time
                process_start = time.time()

                ## Gets the new insertion on Output_oxygen_transport table
                for i in range(len(results)): 

                    ## Get data
                    idoutput = str(results[i][3])
                    query = f"SELECT idsimulation, id_orchestration FROM {self.SQL.mydb.database}.simulation WHERE idsimulation = (SELECT id_simulation FROM {self.poll_table} WHERE id{self.poll_table} = "+idoutput+");"
                    result = self.SQL.mysql_execute(query)
                    idsim = str(result[0][0])
                    idorch = str(result[0][1])

                    ## Get output data
                    query = f'SELECT names_list, path, id_{self.input_table} FROM {self.SQL.mydb.database}.{self.poll_table} WHERE id{self.poll_table} = {str(idoutput)};'
                    result = self.SQL.mysql_execute(query)[0]
                    names_list = result[0]
                    path = result[1]
                    idinput = str(result[2])
                    
                    if names_list != None:
                        print("Remeshing")

                        ## Create a remesh subfolder
                        pathremesh = f'{path}../remeshing/'
                        cmd = f'mkdir {pathremesh}'
                        try:
                            os.system(cmd)
                        except:
                            sys.exit(f'Creation remesh subfolder on {path} failed.')

                        self.prepareRemeshing(pathremesh, path, idinput)

                        self.remesh(pathremesh)                  
                        
                    ## Update tumour simulation time ## should this to be a trigger (when insert output)?
                    query = f'SELECT current_simtime, loop_time, total_simtime, id_initial_input FROM {self.SQL.mydb.database}.orchestration WHERE idorchestration = {str(idorch)};'
                    result = self.SQL.mysql_execute(query)[0]
                    currenttime = result[0]
                    looptime = result[1]
                    totaltime = result[2]
                    idinitial = result[3]
                    query = f'UPDATE {self.SQL.mydb.database}.orchestration SET `current_simtime` = {str(currenttime+looptime)} WHERE idorchestration = {str(idorch)};'
                    self.SQL.mysql_execute_NOreturn(query)

                    print(f'Time {currenttime+looptime} of {totaltime} concluded')
    
                    ## Check for ending
                    if totaltime <= currenttime+looptime or names_list == None: ## orchestration finished

                        ## save final output
                        query = f'INSERT INTO {self.SQL.mydb.database}.final_output(names_list, `path`, datetime, id_initial_input) VALUES (\"{names_list}\", \"{pathremesh}\", NOW(), {str(idinitial)});'
                        self.SQL.mysql_execute_NOreturn(query)
                        ## Finish orchestration
                        query = f'UPDATE {self.SQL.mydb.database}.orchestration SET `status` = "Finished" WHERE idorchestration = {idorch};'
                        self.SQL.mysql_execute_NOreturn(query)
                        ## Excludes from exec_table 
                        query = "DELETE FROM "+self.SQL.mydb.database+".exec_table WHERE provenance_id = "+str(idoutput)+" AND `table_name` = \""+self.poll_table+"\";"
                        self.SQL.mysql_execute_NOreturn(query)
                        break

                    else: 
                        ## Insert new input
                        process_db_start = time.time()
                        self.insertInputsToRestart(idorch, idsim, pathremesh)
                        process_db_stop = time.time()
                        ## Update db time
                        self.DB_time += process_db_stop - process_db_start

                    ## Excludes from exec_table 
                    query = "DELETE FROM "+self.SQL.mydb.database+".exec_table WHERE provenance_id = "+str(idoutput)+" AND `table_name` = \""+self.poll_table+"\";"
                    self.SQL.mysql_execute_NOreturn(query)

                    ## Exclude ANSYS log files
                    cmd = f"rm {path}file*"
                    os.system(cmd)

                ## Stop the stopwatch / counter                     
                process_stop = time.time()                     
                ## Sum the time of the module
                self.total_process += process_stop - process_start 

            else:
                time.sleep(self.poll_time)

    
    ## Copy and write all inputs for remeshing
    def prepareRemeshing(self, pathremesh, path, idinput):

        ## Prepare all input files
        ## write initial_volume and updated_O2 files
        process_db_start = time.time()
        query = f'SELECT provenance_sim FROM {self.SQL.mydb.database}.{self.input_table} WHERE id{self.input_table} = {str(idinput)};'
        provsim = self.SQL.mysql_execute(query)[0][0]
        query = (f'SELECT i_volume, oxygen, vegf, mean_telomer_NB, std_telomer_NB, mean_telomer_SC, std_telomer_SC, mean_apoptotic_NB, std_apoptotic_NB, '
                f'mean_apoptotic_SC, std_apoptotic_SC, mean_necrotic_NB, std_necrotic_NB, mean_necrotic_SC, std_necrotic_SC, mean_diff_NB, std_diff_NB FROM {self.SQL.mydb.database}.relation_element_{provsim} '
                f'WHERE provenance_sim = {str(provsim)};')
        
        process_db_stop = time.time()
        ## Update db time
        self.DB_time += process_db_stop - process_db_start
                        
        result = self.SQL.mysql_execute(query)
        ivol = open(pathremesh+self.nameslist[2],'w')
        uO2 = open(pathremesh+self.nameslist[3],'w')
        vegf = open(pathremesh+self.nameslist[7],'w')
        telomer = open(pathremesh+self.nameslist[8],'w')
        death = open(pathremesh+self.nameslist[9],'w')
        diff = open(pathremesh+self.nameslist[10],'w')
        for r in result:
            ivol.write(str(r[0])+"\n")
            uO2.write(str(r[1])+"\n")
            vegf.write(str(r[2])+"\n")
            telomer.write(f'{str(r[3])} {str(r[4])} {str(r[5])} {str(r[6])}\n')
            death.write(f'{str(r[7])} {str(r[8])} {str(r[9])} {str(r[10])} {str(r[11])} {str(r[12])} {str(r[13])} {str(r[14])}\n')
            diff.write(f'{str(r[15])} {str(r[16])}\n')
        ivol.close()
        uO2.close()
        vegf.close()
        telomer.close()
        death.close()
        diff.close()

        ## copy mesh_nodes, mesh_elements, geometry.stl, updated_cellularity from output mechanical
        cmd = f'cd {path};cp {self.nameslist[0]} {self.nameslist[4]} {self.nameslist[5]} {self.nameslist[6]} ../remeshing/'
        try:
            os.system(cmd)
        except:
            print("Copying files for remeshing failed") 
            
            sys.exit(1)
            
        ## copy and rename updated_volume to volume_f
        cmd = f'cd {path};cp updated_volume.txt ../remeshing/{self.nameslist[1]} '
        try:
            os.system(cmd)
        except:
            print("Copying files for remeshing failed") 
            sys.exit(1)
            
        ## copy initial vascularization
        cmd = f'cd {path}../;cp {self.nameslist[11]} remeshing/'
        try:
            os.system(cmd)
        except:
            print("Copying files for remeshing failed") 
            sys.exit(1)

        ## Save relation table in a file and delete fromthe DB
        ## Save and clean relation_element table
        self.cleanRelation(provsim, path+"/../")


    ## Remeshing
    def remesh(self, pathremesh):
        
        ## Copy remesh script, interpolate script and nodal script
        cp =  f"cp {self.modelspath}/remesh.py {pathremesh}remesh.py; cp {self.modelspath}/interpolate.py {pathremesh}interpolate.py; cp {self.modelspath}/elemental2nodal.py {pathremesh}elemental2nodal.py"
        try:
            os.system(cp)
        except:
            print("Remesh.py and/or interpolate.py not found.") 
            sys.exit(1)
                        

        if self.cluster == "ares":

            ## run all scripts
            certificate = self.proxy
            backend_conf = "'{\"ENDPOINT\":\"https://submit.plgrid.pl\",\"PROXY\":\""+certificate+"\"}'"
            job_conf = ("'{\"script\":\"#!/bin/bash \\n#SBATCH -J remeshing \\n#SBATCH -A plgprimage4-cpu \\n#SBATCH --mem-per-cpu=500MB \\n#SBATCH --time=2:00:00 "
                        "\\n#SBATCH -p plgrid-now \\n#SBATCH --error="+pathremesh+"/error.err \\n#SBATCH --output="+pathremesh+"/output.out"
                        "\\ncd "+pathremesh+" \\nmodule load .plgrid \\nmodule load plgrid/libs/python-numpy/2021.05-intel-2021a \\npip3 install --upgrade pip \\npip3 install pymeshlab"
                        "\\nmodule load vtk/9.1.0-foss-2021b \\npython3 remesh.py > stdremesh.out \\necho Done1. \\nmodule load motif/2.3.8-gcccore-10.3.0"
                        "\\nmodule add ansys/2022R1\\n export I_MPI_FALLBACK=1\\n export I_MPI_FABRICS=\\n export KMP_AFFINITY=disabled\\nansys221 -b -i tumour_mesh.inp -o solver_tumour.out"
                        "\\necho Done2. \\npython3 interpolate.py > stdinterpolation.out\\necho Done3.\", \"host\":\"login01.ares.cyfronet.pl\"}'")

            cmd = f'python3 {os.environ["PLG_GROUPS_STORAGE"]}/plggprimage/Models/hpc-connector/hpc-connector.py --backend Prometheus --backend-conf {backend_conf} job --job-config {job_conf} submit' 
            try:
                os.system(cmd)
            except:
                print(cmd+" FAILED.")
                sys.exit(1)

            ## Wait the job get read
            finished = False
            outputfile = pathremesh+"output.out"
            while finished == False:
                if os.path.isfile(outputfile) == True:
                    out = open(outputfile, 'r')
                    line = out.readlines()
                    if line != []:
                        line = line[-1]
                        if line.startswith("Done3."):
                            finished = True  
        else: 
            ## Copy remesh script and interpolate script
            cp =  f"cp {self.modelspath}/mock_remesh.py {pathremesh}mock_remesh.py"
            try:
                os.system(cp)
            except:
                print("Remesh.py file was not found.") 
                sys.exit(1)

            ## run remesh script
            cmd = "cd "+pathremesh+"; python3 mock_remesh.py"
            try:
                os.system(cmd)
            except:
                print("Remeshing mock script failed.") 
                sys.exit(1)


    ## Returns the content of a file
    def readfile(self, filename, path):

        ## open the file and return the content
        arq = open(path+filename)
        content = arq.readlines()
        content = self.SQL.listtostring(content)
        return content


    ## insert new inputs to restart the cycle
    def insertInputsToRestart(self, idOrch, sim_prov, pathremesh):

        ## Checks if there are input files (provisory)
        filenames = ["death_signals.txt", "differenciation_NB.txt", "i_cellularity.txt", "updated_cellularity.txt", "i_vascularization.txt", "mesh_elements.inp", "mesh_nodes.inp", "telomer_length.txt", "drug_regime.csv", "drugs.is_active.tsv"]

        query = f'SELECT `loop` FROM {self.SQL.mydb.database}.simulation WHERE idsimulation = {sim_prov}'
        newloop = self.SQL.mysql_execute(query)[0][0] + 1

        ## Create directory for the loop
        newPath = self.iofolder+"Orchestration_"+str(idOrch)+"/Loop_"+str(newloop+1)+"/"
        if os.path.isdir(newPath) == False:
            try:
                os.mkdir(newPath)
            except:
                print ("Creation of the directory "+str(newPath)+" failed.")
                exit(1)
            else:
                print ("Successfully created the directory "+str(newPath))  

        ## Copy files to initiate the loop
        for f in filenames[0:8]:
            cmd = f'cp {pathremesh}{f} {newPath}{f}'
            os.system(cmd)
        for f in filenames[8:10]:
            cmd = f'cp {pathremesh}../{f} {newPath}{f}'
            os.system(cmd)
        

        ## Inserts on input_oxygen_transport table
        ## get nelems after remeshing
        nelems = self.getnelems(newPath+filenames[5])
        ## Creates a simulation
        columns = "`id_orchestration`, `description`, `datetime`, `runtime`, `status`, `id_model`, `loop`, nelems"
        data = str(idOrch)+", \"Loop "+str(newloop)+"\", NOW(), 0, \"running\", "+str(self.nextmodel)+", "+str(newloop)+", "+str(nelems)
        self.SQL.mysql_insert("simulation", columns, data) 
        ## Gets the simulation ID
        query = f"SELECT idsimulation FROM {self.SQL.mydb.database}.simulation WHERE `id_orchestration` = {str(idOrch)} AND `description` = \"Loop {str(newloop)}\""
        simID = self.SQL.mysql_execute(query)[0][0]
        ## Create the subfolder
        subdir = "oxygen_transport_"+str(simID)+"/"
        newsubdir= newPath+subdir
        try:
            os.mkdir(newsubdir)
        except:
            print ("Creation of the directory "+str(newPath)+" failed.")

        ## get binsrate
        query = f'SELECT binsrate, nGPUs FROM {self.SQL.mydb.database}.orchestration WHERE idorchestration = {str(idOrch)}'
        result = self.SQL.mysql_execute(query)
        binsrate = result[0][0]
        nGPUs = result[0][1]
        ## Insert into provenance_sim table
        nbins = int(binsrate*nelems)
        if nbins > 0:
            iperjob = (nbins+1)/nGPUs ## first bin runs the biggest ans the maximum elements
        else: 
            iperjob = nelems/nGPUs
        if iperjob < 1:
            iperjob = 1
            nGPUs = 1
        columns = "`idprov`, `inputs_per_job`, `nbins`, `nelems`"
        data = f'{str(simID)}, {iperjob}, {str(nbins)}, {str(nelems)}'
        self.SQL.mysql_insert("provenance_sim", columns, data)
        
        ## Inserts oxygen_tansport input
        columns = "`mesh_elements`, `mesh_nodes`, `cellularity`, `vascularization`, `path`, `datetime`, `provenance_sim`, `id_simulation`"
        files = filenames
        data = "\""+self.readfile(files[5], newPath)+"\", \""+self.readfile(files[6],newPath)+"\", \""+self.readfile(files[2], newPath)+"\",  \""+self.readfile(files[4], newPath)+"\", \""+newsubdir+"\", NOW(), "+str(sim_prov)+", "+str(simID)
        self.SQL.mysql_insert(self.write_table, columns, data) 


    ## Get the number of elements
    def getnelems(self, path):

        arq = open(path,'r')
        lines = arq.readlines()
        nelems = lines[-1].split(',')[1]

        return int(nelems)

    
    ## Save and clean relation_element table
    def cleanRelation(self, sim_prov, path):

        query = f'SELECT * FROM {self.SQL.mydb.database}.relation_element_{sim_prov};'
        relationTable = self.SQL.mysql_execute(query)
        with open(f'{path}/../relation_{sim_prov}.csv', 'w', encoding='UTF8') as f:
            writer = csv.writer(f)
            # write the header
            writer.writerows(relationTable)
        ## Delete relation table
        query = f"DROP TABLE {self.SQL.mydb.database}.relation_element_{sim_prov};"
        self.SQL.mysql_execute_NOreturn(query)