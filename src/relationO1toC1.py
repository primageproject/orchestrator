import os
import sys
import time
import pandas as pd
import pythonSQL as pysql

class O1toC1:

    def __init__(self, dbname, da, al, idnextmodel, cluster, iofolder, poll_time, pwd):

        ## Model name (for prints)
        self.name = "Particularization"
        
        ## Cluster name
        self.cluster = cluster
        
        ## Creates a instance of SQLpython class with a new database connection 
        self.SQL = pysql.SQLpython(cluster, pwd, False, dbname)
        
        ## constant column name
        self.COLUMN_NAME = "value"
        self.COLUMN_BINS = 'eq_spaced'
 
        ## Stopping criterion 
        self.finished = False
        ## Time variables
        self.total_process = 0
        self.elapsed = 0
        self.DB_time = 0
        self.polling_process = 0

        ## Set the interval in seconds to pool
        self.poll_time = poll_time*5
        ## Set next model id
        self.nextmodel = idnextmodel
        ## Set the name of I/O folder
        self.iofolder = iofolder

        ## prov input table
        self.prov_table = 'input_'+da
        ## table name to poll
        self.poll_table = 'output_'+da
        ## table name to write inputs
        self.write_table = 'input_'+al   

        ## Verify if there are interruptions
        query = f"""SELECT id{self.poll_table} FROM {self.SQL.mydb.database}.{self.poll_table} WHERE id_simulation NOT IN (SELECT provenance_sim 
        FROM {self.SQL.mydb.database}.{self.write_table});"""
        interrupteds = self.SQL.mysql_execute(query)

        ## For each interrupted, write on exec_table
        for interrupted in interrupteds:
            query = f"SELECT id{self.poll_table} FROM {self.SQL.mydb.database}.{self.poll_table} WHERE id{self.poll_table} = {str(interrupted[0])};"
            id = self.SQL.mysql_execute(query)[0][0]
            columns = f"`table_name`, `datetime`, `provenance_id`, `action`"
            values = f"\"{self.poll_table}\", NOW(), {id}, 'insert'"
            self.SQL.mysql_insert("exec_table", columns, values)
        
    
    ## Waits a new insertion on 'exec_table'
    def wait(self):
        
        while self.finished == False: 

            ## process time
            process_start_p = time.time()
             
            results = self.SQL.mysql_execute(f"SELECT * FROM {self.SQL.mydb.database}.exec_table WHERE `table_name` = \"{self.poll_table}\" AND action = \"insert\";")

            ## Stop the stopwatch / counter                  
            process_stop_p = time.time()                     
            ## Sum the time of the module
            self.polling_process += process_stop_p - process_start_p
            
            if results != []:
  
                ## process time
                initime = time.time()

                ## Gets the new insertion on Output table
                for i in range(len(results)):      

                    ## Get data simulation data
                    idoutput = str(results[i][3])
                    desc, idsim, idorch, loop, nelems = self.getSimulation(idoutput) 
                    
                    ## Check if simulation finished with success
                    query = f"SELECT status FROM {self.SQL.mydb.database}.simulation WHERE idsimulation = {str(idsim)};"     
                    status = self.SQL.mysql_execute(query)[0][0]
                    ## if failed finish orchestration
                    if status != "Finished":
                        ## Get id orchestration
                        query = f"SELECT id_orchestration FROM {self.SQL.mydb.database}.simulation WHERE idsimulation = {idsim};"
                        idorch = str(self.SQL.mysql_execute(query)[0][0])
                        ## Update orchestrator status
                        query = f"UPDATE {self.SQL.mydb.database}.orchestration SET status = \"Failed.\" WHERE idorchestration = {idorch};"
                        self.SQL.mysql_execute_NOreturn(query)                            
                        print(f'Orchestration ID {idorch} was interrupted.')
                        break
                    
                    process_db_start = time.time()
                    ## Create a relationDB for this table
                    self.addtorelationtable(idsim)
                    process_db_stop = time.time()
                    ## Update db time
                    self.DB_time += process_db_stop - process_db_start
                    
                    print("Particularizing...")
                    
                    ## set files
                    origin_column = ""
                    if desc == "First diffusion":
                        origin_column = "provenance_initial_input"
                    else:
                        origin_column = "provenance_sim"
                        
                    ## Get provenance data
                    path, pathels = self.provenance(origin_column, idsim)
                    
                    ## Get files from the output table
                    oxyfile, volfile = self.outputfiles(idsim)
                    
                    ## Check if do particularization or not
                    query = f'SELECT nbins FROM {self.SQL.mydb.database}.provenance_sim WHERE idprov = {idsim}'
                    self.nbins = int(self.SQL.mysql_execute(query)[0][0])

                    ## Update relation_element table
                    ## Get initial cellularity
                    pathloop = path+"../"
                    ## load elements to memory
                    elements = self.load_elements(pathels)
                    # load values from input files
                    # [oxy, vol, ]
                    paths = [path,path,pathloop,pathloop]

                    ## Set if particularization
                    if self.nbins <= 0:
                        ## DONT DO PARTICULARIZATION - SIMPLY SAVE ALL TO RELATION DATABASE TABLE
                        ## Set nbins = nelems
                        query = f'UPDATE {self.SQL.mydb.database}.provenance_sim SET nbins = nelems WHERE idprov = {idsim};'
                        self.SQL.mysql_execute_NOreturn(query)
                        varP = "oxygen" ## Set a default (not to be used)
                        self.nbins = int(nelems)
                    else: ## Do particularization
                        ## Get variable to particularize
                        query = (f'SELECT varP FROM {self.SQL.mydb.database}.orchestration WHERE idorchestration = '
                                f'(SELECT id_orchestration FROM {self.SQL.mydb.database}.simulation WHERE idsimulation = {idsim});')
                        varP = self.SQL.mysql_execute(query)[0][0]   

                    ## Get data and set data and column to particularize
                    dataP, col, outputs = self.setVariable(oxyfile, volfile, paths, origin_column, varP)   

                    ## Particularize and save on relation table
                    self.particularize(elements, dataP, self.nbins, col, outputs, idorch, idsim, loop, idoutput, path, nelems, varP)  
                    
                    ## Exclude ANSYS log files
                    cmd = f"rm {path}file*"
                    os.system(cmd)                     
                    
                
                ## Excludes from exec_table  
                query = f"DELETE FROM {self.SQL.mydb.database}.exec_table WHERE provenance_id = {str(idoutput)} AND `table_name` = \"{self.poll_table}\";"
                self.SQL.mysql_execute_NOreturn(query) 
                
                # Stop the stopwatch / counter   
                endtime = time.time()                  
                ## Sum the time of the module
                self.total_process += endtime - initime  

            else:
                time.sleep(self.poll_time)  
            
        self.SQL.closeConnection()


    ## Add relation_element table
    def addtorelationtable(self, idsim):

        ## Delete table if exists (for interruption cases)
        query = f"DELETE FROM primage_v4.relation_element_{idsim};"
        try:
            self.SQL.mysql_execute_NOreturn(query)
            print(f"Relation_element_{idsim} table will be re-created.")
        except:
            print(f"Relation_element_{idsim} table will be created.")
        
        if self.cluster != "local":
            path = f'{os.environ["PLG_GROUPS_STORAGE"]}/plggprimage/vph-hfv3-4.0/'
        else:
            path = '../'
        ## Create provenance_sim table
        with open(f'{path}database/relation_O1toC1.sql', 'r') as file:
            sql = file.read().replace('<db_name>', self.SQL.mydb.database)
            sql = sql.replace("<output_table>", self.poll_table)
            sql = sql.replace("<idsim>", str(idsim))
        self.SQL.mysql_execute_NOreturn(sql) 
        print(f"Relation_element_{idsim} table created")
        
    
    ## Get simulation data
    def getSimulation(self, idoutput):
        
        query = f"SELECT description, idsimulation, id_orchestration, `loop`, nelems FROM {self.SQL.mydb.database}.simulation WHERE idsimulation = (SELECT id_simulation FROM {self.poll_table} WHERE id{self.poll_table} = {idoutput});"
        result = self.SQL.mysql_execute(query)
        desc = result[0][0]
        idsim = str(result[0][1])
        idorch = str(result[0][2])
        loop = str(result[0][3])
        nelems = str(result[0][4])
        
        return desc, idsim, idorch, loop, nelems
    
    
    ## Get provenance data (input table related to outputs to particularize)
    def provenance(self, origin_column, idsim):
        
        query = f"SELECT names_list, path, {origin_column} FROM {self.SQL.mydb.database}.{self.prov_table} WHERE id_simulation = "+idsim+";"
        result = self.SQL.mysql_execute(query)
        namesini = result[0][0]
        path = result[0][1]
        elfile = namesini.split(',')[0]
        pathels = path+elfile
        
        return path, pathels
    
    ## Get outputfiles
    def outputfiles(self, idsim):
        
        query = f"SELECT names_list FROM {self.SQL.mydb.database}.{self.poll_table} WHERE id_simulation = "+idsim+";"
        result = self.SQL.mysql_execute(query)
        names = result[0][0]
        oxyfile = names.split(',')[0]
        volfile = names.split(',')[1]
        
        return oxyfile, volfile
    
    
    ## data to particularize
    def setData(self, oxyfile, volfile, origin_column):

        ## set types of template files
        columns1 = ["value"]
        columns2 = ["index", "value"]
        columnscell1 = ["index", "NBalive"] ## cellularity for first loop
        columnscell = ["NBalive", "SCalive", "NBap", "NBnec", "SCap", "SCnec", "count"] ## cellularity after first loop
        #columnstelo = ["meanNB", "stdNB", "meanSC", "stdSC"]
        #columnsdeath = ["meanapNB", "stdapNB", "meannecNB", "stdnecNB", "meanapSC", "stdapSC", "meannecSC", "stdnecSC"]
        #columnsdiff = ["meandiffNB", "stddiffNB"]

        
        ## Set files if first loop
        if origin_column == "provenance_initial_input":
            names = [oxyfile,volfile,"i_cellularity.txt","i_vascularization.txt"]
            columnss = [columns1, columns1, columnscell1, columns2]
        ## Set files if not first loop
        else:
            #names = [oxyfile,volfile,"updated_cellularity.txt","telomer_length.txt","death_signals.txt","differenciation_NB.txt", "i_vascularization.txt"]
            #columnss = [columns1, columns1, columnscell, columnstelo, columnsdeath, columnsdiff, columns2]
            names = [oxyfile,volfile,"updated_cellularity.txt","i_vascularization.txt"]
            columnss = [columns1, columns1, columnscell, columns2]
            
        return names, columnss
    
    
    ## generic function to load other values to pandas and define the name of the column to be referenced
    def load_variable(self, paths, files,columnss):

        i = 0
        variables = [None]*len(paths)
        for i in range(len(paths)):
            # load txt file to pandas data frame
            variables[i] = pd.read_csv(paths[i]+files[i], header=None, sep="\s+|\t", engine="python") ## space separator
            # rename column
            variables[i].columns = columnss[i]
            i+=1

        return variables
    
    
    ## load elemental values to save to the database
    def load_elements(self, PATH_elements):

        try:
            ## read elements file and select the data
            arq = open(PATH_elements,'r')
            lines = arq.readlines()
            if lines[-1] == '\n':
                lines.pop(-1)
            for i in range(0,len(lines)):
                lines[i] = lines[i].split(',')
                lines[i].pop(0)
                lines[i][-1] = lines[i][-1].split('\n')[0]
            # load txt file to pandas data frame - elements
            elements = pd.DataFrame(lines, columns=['Element Number', 'Nodes1', 'Nodes2', 'Nodes3', 'Nodes4'])
            return elements
        except FileNotFoundError:
            print("Failed to load elements")
            exit(1)   
    
    
    ## Select criteria of particularization
    ## The particularization uses 1 variable as criteria to to the selection
    def setVariable(self, oxyfile, volfile, paths, origin_column, varP):
        
        ## Get data to particularize
        names, columnss = self.setData(oxyfile, volfile, origin_column)
        
        ## Load variables
        O2, V, cells, vascu = self.load_variable(paths, names,columnss)
        
        ## list of outputs got from previous model
        outputs = [O2, V, cells, vascu]
        
        if varP == "oxygen":
            dataP = O2
            col = 'value'
        else:
            if varP == "volume":
                dataP = V
                col = 'value'
            else:
                if varP == "cellularity_alive_NB":
                    dataP = cells['NBalive']
                    dataP = dataP.to_frame()
                    col = 'NBalive'
                else:
                    print("The variable to particularize is not registered on particularization method.")
                    sys.exit(1)
                    
        return dataP, col, outputs
    
    
    ## Particularization
    def particularize(self, elems, dataP, nbins, col, outputs, idorch, idsim, loop, idoutput, path, nelems, varP):## DataP is order by element id
        
        ## Binning
        bins, sample = self.calculateBins(dataP, nbins, col)
        print(len(sample))
        
        ## Fills relation table
        process_db_start = time.time()
        self.insert_ALL_Elements_Into_Relation(elems, outputs, bins, idorch, idsim, loop)
        ## get if homogenizing or not
        query = f'SELECT binsrate FROM {self.SQL.mydb.database}.orchestration WHERE idorchestration = {idorch};'
        binsrate = self.SQL.mysql_execute(query)[0][0]

        vascu = outputs[2]
        
        ## if bin 1 has more than 1 element
        query = f'SELECT count(*) FROM {self.SQL.mydb.database}.relation_element_{idsim} WHERE interval_bins = 1;'
        binsize = self.SQL.mysql_execute(query)[0][0]
        menor = False
        if binsize > 1:
            menor = True
            ## Get the two smallest values to use in the first bin
            query = f'''SELECT idrelation_element FROM {self.SQL.mydb.database}.relation_element_{idsim} WHERE 
            `i_cellularity_alive_NB` > 0 OR `i_cellularity_alive_SC` > 0 OR `i_cellularity_apoptotic_NB` > 0 OR 
            `i_cellularity_apoptotic_SC` > 0 OR `i_cellularity_necrotic_NB` > 0  OR `i_cellularity_necrotic_SC` > 0 ORDER BY i_{varP} LIMIT 2;'''
            smaller = self.SQL.mysql_execute(query)[0][0]
            smaller2 = self.SQL.mysql_execute(query)[1][0]
            ## In case all of varP values are equal, if need to ensure the 'min' selected is not equal to the 'max'
            if smaller != smaller2:
                for s in sample:
                    if s == smaller:
                        smaller = smaller2
                        break
            ## Register smaller value as sample and bin -1
            query = (f'UPDATE {self.SQL.mydb.database}.relation_element_{idsim} SET is_sample = 1, interval_bins = -1 WHERE idrelation_element = {smaller};')
            self.SQL.mysql_execute_NOreturn(query)
            sample.append(smaller)
        else:
            if binsrate > 0: ## This is not applyable if no homogenization
                ## update inputs per GPU job
                query = (f'UPDATE {self.SQL.mydb.database}.provenance_sim SET inputs_per_job = inputs_per_job - 1 WHERE idprov = {idsim};')
                self.SQL.mysql_execute_NOreturn(query)

        ## Add BIN_NUM inputs to input table
        self.insertInputsToCell(nbins, idorch, self.nextmodel, idsim, idoutput, path, sample, vascu, nelems, menor)
        process_db_stop = time.time()
        ## Update db time
        self.DB_time += process_db_stop - process_db_start

        ## Free dataframe
        del outputs
        
    
    ## Skip particularization
    def skipParticularization(self, elems, outputs, idorch, idsim, loop, idoutput, path, nelems):
        
        
        ## Fills relation table
        DB_t_start = time.perf_counter()
        self.insert_ALL_Elements_Into_Relation(elems, outputs, [0]*nelems, idorch, idsim, loop)
        DB_t_stop = time.perf_counter() 
        self.DB_time = DB_t_stop - DB_t_start
        
        ## Set all elements as sample
        query = f'SELECT element_number FROM {self.SQL.mydb.database}.relation_element WHERE id_orchestration = {idorch} AND provenance_sim = {idsim}'
        result = self.SQL.mysql_execute(query)
        
        ## transform matrix n x 1 into array
        sample = []
        for s in result:
            sample.append(s[0])
            
        # Add ALL inputs to input table
        vascu = outputs[2]
        self.insertInputsToCell(nelems,idorch,self.nextmodel,idsim,idoutput,path, sample,vascu,nelems)
        
    
    ## Binning
    def calculateBins(self, var, nbins, column):

        nelms = len(var)

        ## Order data    
        var['id'] = var.index+1
        var[column] = pd.to_numeric(var[column], downcast="float")
        var = var.sort_values(by=[column], ascending=True)
        var = var.reset_index()
        var['bin'] = var.index

        ## Calculate indexes
        delta = nelms/nbins
        i = -1 ## Because it is an index
        binsindex = []
        while round(i+delta,2) <= nelms-1:
            i+=delta
            binsindex.append(round(i))
            if len(binsindex) == 1:
                for j in range(binsindex[0]+1):
                    var.at[j,'bin'] = 1
            else:
                incr = len(binsindex)
                for j in range(binsindex[-1]-binsindex[-2]):
                    var.at[j+binsindex[-2]+1,'bin'] = incr

        ## sample the biggest value of each bin
        sample = [] 
        for i in range(nbins):
            sample.append(var.at[binsindex[i],'id'])

        ##Order by id to return to the inicial configuration
        var = var.sort_values(by=['id'], ascending=True)
        bins = var['bin']
        bins = bins.reset_index()

        return bins['bin'], sample
    
    
    ## function that insert the elemental values to 'relation_element' table
    def insert_ALL_Elements_Into_Relation(self,elements,outputs,bins,idOrch,idsim,loop):

        ## unpack outputs
        [O2, V, cells, vascu] = outputs
        
        ## Number of updates in the same time
        batchsize = 5000

        # Loop to insert all the elemental values
        i = 0
        queries = []
        while(i<elements['Element Number'].count()):
            # extract values as string
            elnum = elements.at[i, 'Element Number']
            # the nodes are added as a single string in one field
            elnodes = str(elements.at[i, 'Nodes1'])+" "+str(elements.at[i, 'Nodes2'])+" "+str(elements.at[i, 'Nodes3'])+" "+str(elements.at[i, 'Nodes4'])
            elvol = float(V.at[i,'value']) # elemental volume
            elO2 = O2.at[i,'value'] # elemental oxygen
            evascu = vascu.at[i,'value'] # elemental vascularization
            elcells = [] # elemental cellularity
            
            if loop == '0': ## if initial cellularity
                elcells = [cells.at[i,'NBalive'],0.0, 0.0, 0.0, 0.0, 0.0, 0.0] ## firt index is the initial cellularity and the following ones are zero
            else:
                columns = cells.columns
                elcells = [cells.at[i,columns[0]], cells.at[i,columns[1]], cells.at[i,columns[2]], cells.at[i,columns[3]], cells.at[i,columns[4]], cells.at[i,columns[5]], cells.at[i,columns[6]]] ## get the string line with all the 6 values as a list

            # insert output data to respective columns
            query = f"""INSERT INTO {self.SQL.mydb.database}.`relation_element_{idsim}` (`id_orchestration`,`element_number`,
            `element_nodes`, `i_oxygen`, `i_volume`,`i_vascularization`,`i_cellularity_alive_NB`, `i_cellularity_alive_SC`,
            `i_cellularity_apoptotic_NB`, `i_cellularity_necrotic_NB`, `i_cellularity_apoptotic_SC`, `i_cellularity_necrotic_SC`,
            `i_cells_count`, `interval_bins`,`provenance_sim`) VALUES ({str(idOrch)},{str(elnum)},\'{str(elnodes)}\',{str(round(elO2,6))},
            {str(round(elvol,6))},{str(round(evascu,6))},{str(round(elcells[0],6))}, {str(round(elcells[1],6))}, 
            {str(round(elcells[2],6))}, {str(round(elcells[3],6))}, {str(round(elcells[4],6))}, {str(round(elcells[5],6))},
            {str(elcells[6])}, {str(bins[i])}, {idsim});"""
            queries.append(query)
            if len(queries) == batchsize:
                self.SQL.mysql_executemany_NOreturn(queries)
                queries = []
            i+=1
        self.SQL.mysql_executemany_NOreturn(queries)



    # insert new inputs to Cell model after particularization
    def insertInputsToCell(self, binNum, idOrch, idModel, id_prov, idoutput, path, sample, vascu, nelems, menor = True):

        ## if smaller value is run
        if menor == True:
            binNum += 1

        query = f'SELECT id_simulation from {self.SQL.mydb.database}.{self.poll_table} where id{self.poll_table} = {idoutput} LIMIT 1'
        sim_prov = self.SQL.mysql_execute(query)[0][0]
        query = f'SELECT `loop` FROM {self.SQL.mydb.database}.simulation WHERE idsimulation = {sim_prov}'
        loop = self.SQL.mysql_execute(query)[0][0]

        ## get initial data(in the future form quibim)
        tempregime = open(path+"../drug_regime.csv")
        tempactive = open(path+"../drugs.is_active.tsv")
        linesregime = tempregime.readlines()
        linesactive = tempactive.readlines()

        idelems = ""
        vascu = ""
        
        ## Set sample
        queries = []
        for j in range(binNum):
            id = sample[j]
            query = f'UPDATE {self.SQL.mydb.database}.relation_element_{sim_prov} SET is_sample = 1 where element_number = {str(id)} AND provenance_sim = {str(id_prov)};'
            queries.append(query)
        self.SQL.mysql_executemany_NOreturn(queries)
        
        ## iterate for all the bins
        i = 0
        while (i < binNum):
            id = sample[i] ## element number
            ## set is_sample to true at the respective table column
            idrelation = self.getElement(sim_prov, int(id),idOrch, id_prov)

            VOI = [] ## Values Of Interest
            [VOI] = self.getValuesFromSampleElementCell(sim_prov, idrelation)
            v = VOI[0]
            idelems += str(id)+" "
            vascu += str(v)+"\n"
            i+=1

        ## create simulation for cell model
        desc = self.write_table+" simulation from particularization"
        [newpath,idsim] = self.createSimulationFromElementId(desc,idModel,idOrch,path, sim_prov, loop, nelems)
        ## get all the values to insert new inputs
        
        ## set and execute query to insert the input record for usfd model
        query = f'INSERT INTO {self.SQL.mydb.database}.{self.write_table} (path,datetime,provenance_sim,id_simulation,drug_regime,drug_active,vascularization,id_elements) VALUES (\'{str(newpath)}\', NOW(),{str(sim_prov)},{str(idsim)},\'{self.SQL.listtostring(linesregime)}\',\'{self.SQL.listtostring(linesactive)}\',\'{vascu}\', \'{idelems}\');'
        self.SQL.mysql_execute_NOreturn(query)

        
    def getElement(self, idsim, id, idOrch, id_prov):
        query = f'SELECT idrelation_element FROM {self.SQL.mydb.database}.relation_element_{idsim} WHERE is_sample = 1 AND element_number = {str(id)} AND id_orchestration={str(idOrch)} AND provenance_sim = {str(id_prov)};'
        idrelation = self.SQL.mysql_execute(query)
        return idrelation[0][0]
    
    
    def getValuesFromSampleElementCell(self,idsim, idrelation):
        VOI = []
        query = f"SELECT i_vascularization FROM {self.SQL.mydb.database}.relation_element_{idsim} WHERE idrelation_element = {str(idrelation)};"
        VOI = self.SQL.mysql_execute(query)
        return VOI
    
    
    def createSimulationFromElementId(self, desc, idModel, idOrch, path, sim_prov, loop, nelems):
        
        ## Creates simulation
        query = f'INSERT INTO {self.SQL.mydb.database}.`simulation` (`description`, `datetime`, `runtime`, `status`, `id_model`,`id_orchestration`, `loop`, `nelems`) VALUES (\'{str(desc)}\',NOW(),0,\'started\',{str(idModel)},{str(idOrch)}, {str(loop)}, {str(nelems)});'
        self.SQL.mysql_execute_NOreturn(query)

        ## Gets id new simulation
        query = f"SELECT MAX(idsimulation) FROM {self.SQL.mydb.database}.simulation WHERE `id_orchestration` = {str(idOrch)};"
        idsim = self.SQL.mysql_execute(query)[0][0]

        ## Create subdir to the simulation
        dirs = path.split('/')
        ## Removes the last to items from the dirs var
        del(dirs[-1])
        del(dirs[-1])
        ## Create a new dir

        newpath = ""
        for i in range(len(dirs)):
            newpath += dirs[i]+"/"
        newpath = newpath+"subcellular_"+str(sim_prov)+"/"
        if os.path.isdir(newpath) == False:
            try:
                os.mkdir(newpath)
            except:
                print ("Creation of the directory "+str(newpath)+" failed. Verify if this name file already has been used.")
                exit(1)
            else:
                print ("Successfully created the directory "+str(newpath))  

        return newpath, idsim
        