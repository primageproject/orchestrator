from configparser import Interpolation
import os
import csv
import sys
import time
import json
import numpy as np
import pythonSQL as pysql


class A1toM1:

    def __init__(self, dbname, da, al, prima, idnextmodel, cluster, iofolder, poll_time, pwd):

        ## Model name (for prints)
        self.name = "Homogenization"
        
        ## Cluster name
        self.cluster = cluster

        ## Creates a instance of SQLpython class with a new database connection 
        self.SQL = pysql.SQLpython(cluster, pwd, False, dbname)

        ## constant column name
        self.COLUMN_NAME = 'value'
        self.COLUMN_BINS = 'eq_spaced'
 
        ## Stopping criterion 
        self.finished = False
        ## Time variables
        self.total_process = 0
        self.DB_time = 0
        self.polling_process = 0
        self.elapsed = 0

        ## Set the interval in seconds to pool
        self.poll_time = poll_time*5
        ## Set next model id
        self.nextmodel = idnextmodel
        ## Set the name of I/O folder
        self.iofolder = iofolder

        ## table name of input polled
        self.input_table = 'input_'+da
        ## table name to poll
        self.poll_table = 'output_'+da
        ## table name to write inputs
        self.write_table = 'input_'+al 
        ## output table before particularization
        self.prov_table = 'output_'+prima   
        
        ## folder name next model
        self.al = al

        ## Columns of the database to homogenize
        self.columns = ["volume", "volume_ratio", "oxygen", "cellularity_alive_NB", "cellularity_alive_SC", "cellularity_apoptotic_NB", "cellularity_necrotic_NB",
         "cellularity_apoptotic_SC", "cellularity_necrotic_SC", "vegf", "mean_telomer_NB", "std_telomer_NB", "mean_telomer_SC", "std_telomer_SC", "mean_apoptotic_NB",
         "std_apoptotic_NB", "mean_apoptotic_SC", "std_apoptotic_SC", "mean_necrotic_NB", "std_necrotic_NB", "mean_necrotic_SC", "std_necrotic_SC", "mean_diff_NB", "std_diff_NB", "cells_count"]
        
        ## Verify if there are interruptions
        query = f"""SELECT provenance_sim, count(*) FROM {self.SQL.mydb.database}.{self.input_table} WHERE id{self.input_table} IN 
        (SELECT id_{self.input_table} FROM {self.SQL.mydb.database}.{self.poll_table}) GROUP BY provenance_sim;"""
        provsims1 = self.SQL.mysql_execute(query)
        provsimsready = []
        for p in provsims1:
            if p != None:
                provsim = int(p[0])
                noutputsABM = int(p[1])
                ## Check if all outputsA1 were inserted
                query = f"SELECT nbins+1 FROM {self.SQL.mydb.database}.provenance_sim WHERE idprov = {provsim};"
                ngoal = self.SQL.mysql_execute(query)[0][0]
                if ngoal == noutputsABM:
                    provsimsready.append(provsim)
        query = f"SELECT provenance_sim FROM {self.SQL.mydb.database}.{self.write_table};"
        provsims2 = self.SQL.mysql_execute(query)
        provsimsfinished = []
        for p in provsims2:
            if p[0] != None:
                provsimsfinished.append(int(p[0]))                       
        provsiminterrupteds = list(set(provsimsready) - set(provsimsfinished))
        provsstring = self.SQL.listtostring(provsiminterrupteds,True,False)
        if provsiminterrupteds != []:
            query = f"""SELECT id{self.poll_table} FROM {self.SQL.mydb.database}.{self.poll_table} WHERE id_{self.input_table} IN 
            (SELECT id{self.input_table} FROM {self.SQL.mydb.database}.{self.input_table} WHERE provenance_sim IN ({provsstring}));"""
            interrupteds = self.SQL.mysql_execute(query)
        else:
            interrupteds = []

        ## For each interrupted, write on exec_table
        for interrupted in interrupteds:
            query = f"SELECT id{self.poll_table} FROM {self.SQL.mydb.database}.{self.poll_table} WHERE id{self.poll_table} = {str(interrupted[0])};"
            id = self.SQL.mysql_execute(query)[0][0]
            columns = f"`table_name`, `datetime`, `provenance_id`, `action`"
            values = f"\"{self.poll_table}\", NOW(), {id}, 'insert'"
            self.SQL.mysql_insert("exec_table", columns, values)


    def wait(self):

        while self.finished == False: 

            ## process time
            process_start_p = time.time()

            results = self.SQL.mysql_execute(f"SELECT * FROM {self.SQL.mydb.database}.exec_table WHERE `table_name` = \"{self.poll_table}\" AND action = \"insert\";")

            ## Stop the stopwatch / counter                 
            process_stop_p = time.time()                     
            ## Sum the time of the module
            self.polling_process += process_stop_p - process_start_p
            
            if results != []:
                
                ids = ""
                for r in results:
                    ids = ids+str(r[3])+","
                ids = ids[:-1]
                ## Get provenance_sims
                query = (f'SELECT DISTINCT provenance_sim FROM {self.SQL.mydb.database}.{self.input_table} WHERE id{self.input_table} IN'
                         f'(SELECT id_{self.input_table} FROM {self.SQL.mydb.database}.{self.poll_table} WHERE id{self.poll_table} IN ({ids}));')
                provs = self.SQL.mysql_execute(query)
                
                ## Gets the new insertion on Output_oxygen_transport table
                for p in provs:
                    
                    ## Get provenance sim id
                    prov = p[0]    
                    
                    ## Get all id outputs
                    query = (f'SELECT id{self.poll_table} FROM {self.SQL.mydb.database}.{self.poll_table} WHERE id_{self.input_table} ' 
                             f'IN (SELECT id{self.input_table} FROM {self.SQL.mydb.database}.{self.input_table} WHERE provenance_sim = {prov})')
                    idsout = self.SQL.mysql_execute(query)
                    
                    ## Get number of inputs expected
                    query = f'SELECT count(*) FROM {self.SQL.mydb.database}.{self.input_table} WHERE provenance_sim = {prov}'
                    ninputs = self.SQL.mysql_execute(query)[0][0]
                    
                    ## check if all outputs are ready
                    if ninputs == len(idsout):
                        
                        ## process time
                        initime = time.time() 
                    
                        ## Get outputs failed
                        idsoutstring = ""
                        for id in idsout:
                            idsoutstring = idsoutstring+str(id[0])+","
                        idsoutstring = idsoutstring[:-1]
                        
                        query = (f'SELECT id{self.poll_table} FROM {self.SQL.mydb.database}.{self.poll_table} WHERE names_list IS NULL AND id{self.poll_table} '
                                f'IN ({idsoutstring})')
                        idsfailed = self.SQL.mysql_execute(query)

                        ## If some failure
                        if idsfailed != []:
                            for id in idsfailed:
                                self.rebinning(id[0], prov)

                        ## Homogenize    
                        process_db_start = time.time()       
                        ## Get outputs and save them in the relaiton table
                        valid = self.getOutputs(prov)
                        process_db_stop = time.time()
                        ## Update db time
                        self.DB_time += process_db_stop - process_db_start
                        
                        print(self.total_process)
                        print(self.DB_time)
                        print('\n')

                        ##Update number of inputs
                        ninputs = ninputs - len(idsfailed)
    
                        ## HOMOGENIZATION                            
                        if ninputs > 1 and valid == True: ## If more than 1 bin has run with success
                            print("Homogenizing...")
                            self.interpolate(prov, ninputs)  
                            
                        print(self.total_process)
                        print(self.DB_time)
                        print('\n')                          

                        ## write new input to input table
                        ## get one idsim for geting the path and the current loop
                        query = f'SELECT id_simulation FROM {self.SQL.mydb.database}.{self.poll_table} WHERE id{self.poll_table} = {idsout[0][0]}'
                        idsim = self.SQL.mysql_execute(query)[0][0]
                        new_path = self.addInputNext(idsim, prov, valid) ## idsim = any idsim of the outputs homogenized

                        ## Delete from exec table
                        queries = []
                        for idoutput in idsout:
                            query = "DELETE FROM "+self.SQL.mydb.database+".exec_table WHERE provenance_id = "+str(idoutput[0])+" AND `table_name` = \""+self.poll_table+"\" AND `action` = \"insert\";"
                            queries.append(query)
                        self.SQL.mysql_executemany_NOreturn(queries)
                
                        # Stop the stopwatch / counter 
                        endtime = time.time()                     
                        ## Sum the time of the module
                        self.total_process += endtime - initime
                        
                        print(self.total_process)
                        print(self.DB_time)
                        print('\n')
                
            else:
                time.sleep(self.poll_time)
                
    ## Ready outputs and save them in the database (relation table)
    def getOutputs(self,sim_prov):
        
        idsoutput = []
        files = []
        idselem = []
        
        query = (f'SELECT idoutput_ABM, output_ABM.names_list, output_ABM.path, id_element FROM {self.SQL.mydb.database}.{self.poll_table} INNER JOIN {self.SQL.mydb.database}.{self.input_table} ON {self.poll_table}.id_input_ABM = {self.input_table}.idinput_ABM '
                 f'WHERE provenance_sim = {sim_prov} and {self.poll_table}.names_list IS NOT NULL;')
        results = self.SQL.mysql_execute(query)
        for r in results:
            idsoutput.append(r[0])
            files.append(str(r[2])+str(r[1]))
            idselem.append(r[3])
            
        valid = self.saveOutputsToRelation(files, idselem, sim_prov)
        return valid

    ## get all abm inputs and element ids
    def saveOutputsToRelation(self, files, idselem, sim_prov):
        
        queries = []
        validratios = [] ## list with volumes bigger than zero
        valid = True
        
        for i in range(len(idselem)):
            
            ## Read output file
            output = open(files[i])
            values = json.load(output)
            volume = values["primage"]["tumour_volume"]
            volratio = values["primage"]["total_volume_ratio_updated"]
            oxygen = values["primage"]["O2"]
            celluAliveNB = values["primage"]["cellularity"][0]
            celluAliveSC = values["primage"]["cellularity"][1]
            celluApopNB = values["primage"]["cellularity"][2]
            celluApopSC = values["primage"]["cellularity"][3]
            celluNecroNB = values["primage"]["cellularity"][4]
            celluNecroSC = values["primage"]["cellularity"][5]
            vegf = values["primage"]["ratio_VEGF_NB_SC"]
            meanTeloNB = values["primage"]["nb_telomere_length_mean"]
            stdTeloNB = values["primage"]["nb_telomere_length_sd"]
            meanTeloSC = values["primage"]["sc_telomere_length_mean"]
            stdTeloSC = values["primage"]["sc_telomere_length_sd"]
            meanApopNB = values["primage"]["nb_apop_signal_mean"]
            stdApopNB = values["primage"]["nb_apop_signal_sd"]
            meanNecroNB = values["primage"]["nb_necro_signal_mean"]
            stdNecroNB = values["primage"]["nb_necro_signal_sd"]
            meanApopSC = values["primage"]["sc_apop_signal_mean"]
            stdApopSC = values["primage"]["sc_apop_signal_sd"]
            meanNecroSC = values["primage"]["sc_necro_signal_mean"]
            stdNecroSC = values["primage"]["sc_necro_signal_sd"]
            meanDiff = values["primage"]["extent_of_differentiation_mean"]
            stdDiff = values["primage"]["extent_of_differentiation_sd"]
            cellscount = values["primage"]["cell_count"]
            
            ## Thresolding
            ## Fix cellularities, volume ratio and cell count if vratio is negative
            if volratio < 0.15:
                query = f'SELECT i_cells_count, i_volume FROM {self.SQL.mydb.database}.relation_element_{sim_prov} WHERE idrelation_element = {idselem[i]};'
                result = self.SQL.mysql_execute(query)
                icellscount = result[0][0]
                ivol = result [0][1]
                ecm = values["primage"]["ecm"]
                celluAliveNB = (1-ecm)/2
                celluAliveSC = (1-ecm)/2
                if icellscount > 0:
                    cellscount = -int(icellscount)
                else:
                    totalcellu = celluAliveNB + celluAliveSC + celluApopNB + celluApopSC + celluNecroNB + celluNecroSC
                    cellscount = -int(ivol*totalcellu*9.39*10**4)##tumour density = 9.39e-05 [cells/micrometer 3]
            else:
                validratios.append(float(volratio))
            
            query = (f'UPDATE {self.SQL.mydb.database}.relation_element_{sim_prov} SET `volume` = {volume}, `volume_ratio` = {volratio},'
                     f'`oxygen` = {oxygen}, `cellularity_alive_NB` = {celluAliveNB}, `cellularity_alive_SC` = {celluAliveSC}, '
                     f'`cellularity_apoptotic_NB`  = {celluApopNB}, `cellularity_apoptotic_SC` = {celluApopSC}, `cellularity_necrotic_NB` = {celluNecroNB}, '
                     f'`cellularity_necrotic_SC` = {celluNecroSC}, `vegf` = {vegf}, `mean_telomer_NB` = {meanTeloNB}, `std_telomer_NB` = {stdTeloNB}, '
                     f'`mean_telomer_SC` = {meanTeloSC}, `std_telomer_SC` = {stdTeloSC}, `mean_apoptotic_NB` = {meanApopNB}, `std_apoptotic_NB` = {stdApopNB},'
                     f'`mean_apoptotic_SC` = {meanApopSC}, `std_apoptotic_SC` = {stdApopSC}, `mean_necrotic_NB` = {meanNecroNB}, `std_necrotic_NB` = {stdNecroNB},'
                     f'`mean_necrotic_SC` = {meanNecroSC}, `std_necrotic_SC` = {stdNecroSC}, `mean_diff_NB` = {meanDiff}, `std_diff_NB` = {stdDiff},'
                     f' cells_count = {cellscount} WHERE idrelation_element = {idselem[i]};')
            
            queries.append(query)

        self.SQL.mysql_executemany_NOreturn(queries)

        median = -1
        ## calculate median of valid volumes
        print(f"Valid volume ratios: {validratios}")
        if validratios != []:
            vratioarray = np.array(validratios)
            median = np.percentile(vratioarray, 50)
            print(f'Median: {median}')
        else: 
            print("All volume ratios are below 0.15. The multiscale model will not converge.")
            valid = False

        ## Update relation table where volume is negative
        query = f'UPDATE {self.SQL.mydb.database}.relation_element_{sim_prov} SET `volume_ratio` = {median}, cells_count = cells_count*-{median} WHERE `volume_ratio` < 0.15;'
        self.SQL.mysql_execute_NOreturn(query)

        return valid

    
    ## interpolate values of one output variable
    def interpolate(self,sim_prov, nbins):

        ## For each bin, get bin count
        query = f'SELECT count(*) FROM {self.SQL.mydb.database}.relation_element_{sim_prov} WHERE provenance_sim = {sim_prov} GROUP BY interval_bins ORDER BY interval_bins;'
        binscount = self.SQL.mysql_execute(query)

        ## Get max bin values
        query = (f'SELECT {self.SQL.listtostring(self.columns, comma=True, space=True)} FROM {self.SQL.mydb.database}.relation_element_{sim_prov} '
                 f'WHERE provenance_sim = {sim_prov} AND is_sample = 1 ORDER BY interval_bins;')
        outputs = self.SQL.mysql_execute(query)

        ## Initialize delta matrix (nbins x ncolumns)
        deltas = [None]*(nbins-1) ## Because the bin = -1 is not a bin. It is only used to iterpolate with bin 1
        for d in range(len(deltas)):
            deltas[d] = [None]*len(self.columns)

        for j in range(len(self.columns)):
            for i in range(1,nbins):
                if binscount[i][0] > 1:
                    ## Calculate delta
                    value_bin_1 = outputs[i-1][j]##Output[0] refers to bin -1
                    value_bin_2 = outputs[i][j]
                    deltas[i-1][j] = (value_bin_2 - value_bin_1)/binscount[i][0]
                else:
                    deltas[i-1][j] = 1

        values = self.linearInterpolation(outputs[1:], binscount[1:], deltas)
        
        process_db_start = time.time() 
        ## save interpolated values on DB
        self.saveToRelation(sim_prov, values, self.columns)
        process_db_stop = time.time()
        ## Update db time
        self.DB_time += process_db_stop - process_db_start

    
    ## Do a linear interpolation for all the bin (ncolums)
    ## Return only homogenized values in order or particularization
    def linearInterpolation(self, outputs, binscounts, deltas):

        ## create matrix
        ncolumns = len(deltas[0])
        ## Initialize matrix of interpolation
        ninterpolations = 0
        for i in range(len(binscounts)):
            ninterpolations += binscounts[i][0]-1 ## -1 because the element of the bin was simulated will not be interpolated
        interpolated = []
        for k in range(ninterpolations):
            interpolated.append([None]*ncolumns)

        ## Interpolation
        currentbin = 0
        for i in range(len(binscounts)):
            binsize = binscounts[i][0]-1 ## -1 because of element of the bin was simulated
            for j in range(ncolumns):
                ##  Value calculated of the bin
                calculatedValue = outputs[i][j]
                for k in range(binsize):
                    interpolated[currentbin+k][j] = (calculatedValue-(deltas[i][j]*(binsize-k)))
            currentbin += binsize

        return interpolated

    
    ## Save interpolated values to DB
    def saveToRelation(self, sim_prov, values, columns):
        
        ## Get variable of particularization
        query = (f'SELECT varP FROM {self.SQL.mydb.database}.orchestration WHERE idorchestration = '
                f'(SELECT id_orchestration FROM {self.SQL.mydb.database}.simulation WHERE idsimulation = {sim_prov});')
        varP = self.SQL.mysql_execute(query)[0][0] 

        ## Number of updates in the same time
        batchsize = 3000

        ## Get elements IDs to insert (in order of insertion, that is, ordered by the column of particularization)
        query = f'SELECT idrelation_element FROM {self.SQL.mydb.database}.relation_element_{sim_prov} WHERE is_sample = 0 ORDER BY interval_bins, i_{varP}, element_number;'
        idselem = self.SQL.mysql_execute(query)

        ## queries
        queries = []
        count = 0
        for i in range(len(idselem)):
            sets = "SET " ## Stringwith all set values
            for j in range(len(columns)):
                sets += columns[j]+" = "+str(values[i][j])+", "
            sets = sets[:-2]
            idelem = idselem[i][0]
            query = f'UPDATE {self.SQL.mydb.database}.relation_element_{sim_prov} {sets} WHERE idrelation_element = {idelem};'
            queries.append(query)
            count += 1
            if count == batchsize:
                self.SQL.mysql_executemany_NOreturn(queries)
                count = 0
                queries = []
        self.SQL.mysql_executemany_NOreturn(queries)
        print("Homogenization finished.")


    ## insert one single input to unizar input table 
    ## based on the homogenized outputs
    def addInputNext(self, idsim_output, sim_prov, valid):

        ## If vratio values are valid, go on
        if valid == True:
            ## Get data needed
            query = (f'SELECT volume_ratio, cellularity_alive_NB, cellularity_alive_SC, cellularity_apoptotic_NB, cellularity_necrotic_NB, cellularity_apoptotic_SC, '
                    f'cellularity_necrotic_SC, cells_count FROM {self.SQL.mydb.database}.relation_element_{sim_prov} ORDER BY element_number;')
            values = self.SQL.mysql_execute(query)
            
            ## Prepare values
            volratio = []
            cellu = []
            for v in values:
                volratio.append(str(v[0])+"\n")
                #volratio.append(str(0.8)+"\n")
                cellu.append(f'{str(v[1])} {str(v[2])} {str(v[3])} {str(v[4])} {str(v[5])} {str(v[6])} {str(v[7])}\n')

            new_idsim, new_path = self.createSimulation(idsim_output, sim_prov)
            ## Insert input
            query = (f'INSERT INTO {self.SQL.mydb.database}.{self.write_table} (`path`,`datetime`,`provenance_sim`, `id_simulation`, `cellularity`, `volume_ratio`) '
                    f'VALUES (\"{new_path}\",NOW(),{str(sim_prov)},{str(new_idsim)}, \"{self.SQL.listtostring(cellu)}\", \"{self.SQL.listtostring(volratio)}\");')
        else:## Return error
            new_idsim, new_path = self.createSimulation(idsim_output, sim_prov)
            ## Insert input
            query = (f'INSERT INTO {self.SQL.mydb.database}.{self.write_table} (`path`,`datetime`,`provenance_sim`, `id_simulation`, `cellularity`, `volume_ratio`) '
                    f'VALUES (\"{new_path}\",NOW(),{str(sim_prov)},{str(new_idsim)}, \"NULL\", \"NULL\");')
       
        ## execute the query to insert new input
        self.SQL.mysql_execute_NOreturn(query)

        return new_path


    ## Create new simulation of DB
    def createSimulation(self, idsim, provsim):

        ## Get output path
        query = f"SELECT path FROM {self.SQL.mydb.database}.{self.poll_table} WHERE id_simulation = {str(idsim)}"
        path = self.SQL.mysql_execute(query)[0][0]

        ## get loop number
        query = f'SELECT `loop`, nelems, id_orchestration FROM {self.SQL.mydb.database}.simulation WHERE idsimulation = {str(idsim)}' 
        results = self.SQL.mysql_execute(query)
        loop = results[0][0]
        nelems = results[0][1]
        idorch = results[0][2]
        
        ## Get nbins
        query = f"SELECT nbins FROM {self.SQL.mydb.database}.provenance_sim WHERE idprov = {str(provsim)}"
        nbins = self.SQL.mysql_execute(query)[0][0]
        
        if nbins > 0:
            desc = 'Mechanical simulation from homogenization'
        else:
            desc = 'Mechanical simulation (without homogenization)'
            
        ## Create subdir to the simulation
        dirs = path.split('/')
        ## Removes the last to items from the dirs var
        del(dirs[-1])
        del(dirs[-1])        

        query = (f'INSERT INTO {self.SQL.mydb.database}.`simulation` (`description`, `datetime`, `runtime`, `status`, `id_model`,`id_orchestration`, `loop`, '
                f'nelems) VALUES (\'{desc}\', NOW(),0,\'started\',{str(self.nextmodel)},{str(idorch)}, {str(loop)}, {str(nelems)});')
        self.SQL.mysql_execute_NOreturn(query)
        query = f'SELECT MAX(idsimulation) FROM {self.SQL.mydb.database}.simulation where id_orchestration = {str(idorch)} and id_model = {str(self.nextmodel)};'
        idsim = str(self.SQL.mysql_execute(query)[0][0])

        newPath = f'{self.iofolder}{dirs[-2]}/{dirs[-1]}/{self.al}_{str(idsim)}/'
        if os.path.isdir(newPath) == False:
            try:
                os.mkdir(newPath)
            except:
                print ("Creation of the directory "+str(newPath)+" failed. Verify if this name file already has been used.")
                exit(1)
            else:
                print ("Successfully created the directory "+str(newPath))  

        return idsim, newPath
        
    
    ## Re-binning in case of failures
    def rebinning(self, idelem, prov):
        
        ## Get eleement id
        query = (f'SELECT id_element FROM {self.SQL.mydb.database}.{self.input_table} WHERE id{self.input_table} = '
                f'(SELECT id_{self.input_table} FROM {self.SQL.mydb.database}.{self.poll_table} WHERE id{self.poll_table} = {idelem});')
        elem = self.SQL.mysql_execute(query)[0][0]
        ## Unset id as sample
        query = f'UPDATE {self.SQL.mydb.database}.relation_element_{prov} SET is_sample = 0 WHERE idrelation_element = {elem};'
        self.SQL.mysql_execute_NOreturn(query)
        ## Get bin to remove
        query = f'SELECT interval_bins FROM {self.SQL.mydb.database}.relation_element_{prov} WHERE idrelation_element = {elem};'
        bin = self.SQL.mysql_execute(query)[0][0]

        if bin > 0: ## If it is possible to join 2 bins
            ## Re-binning
            ## Join 2 bins and reduce next bins
            query = f'UPDATE {self.SQL.mydb.database}.relation_element_{prov} SET interval_bins = interval_bins-1 WHERE interval_bins > {bin};'
            self.SQL.mysql_execute_NOreturn(query)
            print(f"Bin {bin} was joined to bin {bin-1}.")
        else:## If smaller value failed
            ## Get values of bin 1
            query = f'SELECT volume, oxygen, vegf, mean_telomer_NB, std_telomer_NB, mean_telomer_SC, std_telomer_SC, mean_apoptotic_NB, std_apoptotic_NB, '
            f'mean_apoptotic_SC, std_apoptotic_SC, mean_necrotic_NB, std_necrotic_NB, mean_necrotic_SC, std_necrotic_SC, mean_diff_NB, std_diff_NB, volume_ratio, cellularity_alive_NB, cellularity_alive_SC, '
            f'cellularity_apoptotic_NB, cellularity_apoptotic_SC, cellularity_necrotic_NB, cellularity_necrotic_SC {self.SQL.mydb.database}.relation_element_{prov} '
            f'WHERE is_sample = 1 and interval_bins = 1;'
            values = self.SQL.mysql_execute(query)
            ## Copies values of bin 1 to bin -1
            query = (f'UPDATE {self.SQL.mydb.database}.relation_element_{prov} SET i_volume = {values[0][0]}, oxygen = {values[0][1]}, vegf = {values[0][2]}, '
                        f'mean_telomer_NB = {values[0][3]}, std_telomer_NB = {values[0][4]}, mean_telomer_SC = {values[0][5]}, std_telomer_SC = {values[0][6]}, '
                        f'mean_apoptotic_NB = {values[0][7]}, std_apoptotic_NB = {values[0][8]}, mean_apoptotic_SC = {values[0][9]}, std_apoptotic_SC = {values[0][10]}, '
                        f'mean_necrotic_NB = {values[0][11]}, std_necrotic_NB = {values[0][12]}, mean_necrotic_SC = {values[0][13]}, std_necrotic_SC = {values[0][14]}, '
                        f'mean_diff_NB = = {values[0][15]}, std_diff_NB = {values[0][16]}, '
                        f'volume_ratio = {values[0][17]}, cellularity_alive_NB = {values[0][18]}, cellularity_alive_SC = {values[0][19]}, '
                        f'cellularity_apoptotic_NB = {values[0][20]}, cellularity_apoptotic_SC = {values[0][21]}, cellularity_necrotic_NB = {values[0][22]}, '
                        f'cellularity_necrotic_SC = {values[0][23]} WHERE is_sample = 1 and interval_bins = -1;')
            
        ## Update nbins
        query = f"UPDATE {self.SQL.mydb.database}.provenance_sim SET nbins = nbins-1 WHERE idprov = {prov};"
        self.SQL.mysql_execute_NOreturn(query)
