import os
import sys
import time
import json
import cProfile
import requests
import traceback
import threading
import serializer
import deserializer
import relationO1toC1
import relationC1toA1
import relationA1toM1
import relationM1toO1
import pythonSQL as pysql

class MasterCommander:
    
    def __init__(self):
        
        ## CLASS VARIABLES
        ## set cluster
        self.cluster = "ares"
        ## to print
        self.debug = True
        ## DB name
        self.dbname = "primage_v4"
        ## poll time
        self.poll_time = 3
        ## Set models path
        if self.cluster == "ares":
            self.modelspath = os.environ["PLG_GROUPS_STORAGE"]+"/plggprimage/Models/"
        else:
            self.modelspath = '/home/osboxes/Documents/Models/'    
        ## initial files
        ## Last thre are for the cases where it starts after the first cycle
        self.iifiles = ["i_cellularity.txt", "i_vascularization.txt", "mesh_elements.inp", "mesh_nodes.inp", "drugs.is_active.tsv",
                        "telomer_length.txt", "death_signals.txt", "differenciation_NB.txt"]  

        ## Stopping criterion 
        self.finished = False

        ## Initialization file
        self.inifile = "initialization.json"
        if self.cluster == "local":
            ## paths
            self.scratchpath = "../../"
            self.onedatapath = self.scratchpath+"onedata/" ## where onedata dbs are mounted
            self.outputspath = self.scratchpath+"outputs/" ## where result must to be saved
            self.inputspath = self.scratchpath+"inputs/" ## where the initial inputs and all the execution happen
            ## database password
            self.pwd = "password"
            self.quibimtoken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI2NDViNjQ3YTgzNDJiYTA4M2RjMjZmMWMiLCJpYXQiOjE2ODQ0OTE4OTIsImV4cCI6MTY4NDY2NDY5Mn0.DqKxiL41oOpgTq1KJlEP7_HTfiIRro0kHY_ljKzYLYs"
        else:
            ## paths
            self.scratchpath = os.environ["SCRATCH"]+"/orchestrator-"+os.environ["SLURM_JOB_ID"]+"/"
            self.onedatapath = self.scratchpath+"onedata/" ## where onedata dbs are mounted
            self.outputspath = self.scratchpath+"outputs/" ## where result must to be saved
            self.inputspath = self.scratchpath+"inputs/" ## where the initial inputs and all the execution happen
            ## database password
            self.pwd = os.environ["DB_PASSWORD"]
            self.proxy = os.environ["PROXY"]
            ## oneclient access
            self.onetoken = os.environ["ONECLIENT_ACCESS_TOKEN"]
            self.onehost = os.environ["ONECLIENT_PROVIDER_HOST"]
            try:
                self.quibimtoken = os.environ["QUIBIM_ACCESS_TOKEN"]
            except:
                self.quibimtoken = ""
        
        ## Read topology
        self.readTopology()

        ## Initialize DB class
        self.SQL = pysql.SQLpython(self.cluster, self.pwd, self.debug)
        
        ## Check if db exist and connect to it or initialize it
        self.SQL.connectDB(self.dbname)
        
        ## Check if all the models exist in the DB
        self.checkModels()

        ## Clean exec_table
        self.dropexec()
        
        ## Create DB
        self.SQL.createTablesDB(self.models, self.names)
        
        ## Insert triggers
        self.SQL.insertTriggers(self.models, self.names)
        
        ## Initialize modules
        self.inimodules()

        ## Read analysis json, initialize file system, run preprocessor and starts simulation
        self.newinput()

        ## Get unfinished orchestrations
        self.ongoingids = self.SQL.mysql_execute(f"select idorchestration from {self.SQL.mydb.database}.orchestration WHERE status = \"Running\";")

        
    def main(self):
        
        ## Initialize modules
        serit = [] ## Serializer threads
        desert = [] ## De-serializer threads
        relt = [] ## Relation threads
        for i in range(len(self.names)):
            try:
                serit.append(threading.Thread(target=self.ms[i].wait, args=(), daemon=True))
                serit[i].start()
            except:
                print("Failed on creating thread for the "+self.names[i]+" serializer.")
                sys.exit(1)
            try:
                desert.append(threading.Thread(target=self.md[i].wait, args=(), daemon=True))
                desert[i].start()
            except:
                print("Failed on creating thread for the "+self.names[i]+" de-serializer.")
                sys.exit(1)
        for i in range(len(self.relations)):
            try:
                relt.append(threading.Thread(target=self.mr[i].wait, args=(), daemon=True))
                relt[i].start()
            except:
                print("Failed on creating thread for the "+self.relations[i]+" relation module.")
                sys.exit(1)
                

        while self.finished == False:

            time.sleep(5)
            self.checkEnd()
    
        
    ## Read topology
    def readTopology(self):
    
        ## Read file
        try:
            topofile = open("initialization.json")
            topology = json.load(topofile)
        except:
            print(f"Initialization file not detected. Check if the JSON format is correct or if the file is in the folder {self.scratchpath}")
            sys.exit(1)

        ## Orchestrator modules params
        try:
            self.binsrate = topology["binsrate"]
            self.totaltime = topology["total_time"]
            self.loopsize = topology["loop_size"]
            self.initialtime = topology["initial_time"]
            self.nGPUs = topology["nGPUs"]
            self.varP = topology["varP"]
            self.calibration = topology["calibration_file"]
        except:
            print("Content of topology file is not correct.")
            traceback.print_exc()
            sys.exit(1)
        
        ## Read topology
        self.names = []
        self.models = []
        self.relations = []
        i = 1
        exist = True
        while exist == True:
            try:
                self.names.append(topology["name_module_"+str(i)])
                self.models.append(topology["model_"+str(i)])
                i += 1
            except:
                exist = False
        i = 1
        exist = True
        while exist == True:
            try:
                self.relations.append(topology["name_relation_"+str(i)])
                i += 1
            except:
                exist = False

    ## Read analysis json
    def newinput(self):

        ## Read file
        try:
            analysisfile = open(self.inputspath+"analysis.json")
            analysis = json.load(analysisfile)

            ## Get user user and patient data
            if self.quibimtoken == "":
                self.quibimtoken2 = analysis["token"]
            self.code = analysis["patient"]["identifier"]
            self.project = "Neuroblastoma"
            self.orchname = analysis["patient"]["name"]

            ## Copy files to HPC
            for serie in analysis["series"]:
                folderwrite = serie["folder"]
                folderget = ""
                if self.cluster != "local":
                    folderget = folderwrite.replace("study",os.environ["INPUTS"]+"../../")
                else:
                    folderget = f"/PRIMAGEDATALAKE{folderwrite}"
                cmd = f"mkdir -p {self.inputspath}{self.orchname}/{folderwrite}"
                os.system(cmd)
                if self.cluster != "local":
                    cmd = f'cp -rf {self.onedatapath}{folderget}/* {self.inputspath}{self.orchname}{folderwrite}'
                else:
                    cmd = f'cp -rf {self.onedatapath}{folderget}/* {self.inputspath}{self.orchname}{folderwrite}'
                try:
                    os.system(cmd)
                except:
                    print(f"Failed to copy to {self.inputspath} from datalake.")
                    sys.exit(1) 
            print("files copied")
            ## Run preprocessor
            self.preprocessor()
            print("pre-processor finished")

            ## Trigger execution
            self.startSimulation()
            print("input registered")

        except:
            print(f"Preprocessor failed. If a new input should be added, please check if the JSON format is correct. The file should be named 'analysis.json' and be in the folder {self.inputspath}")


    ## Run preprocessor container
    def preprocessor(self):

        ## Create /study /analysis and /outputs for preprocessor
        cmd = f"mkdir {self.inputspath}{self.orchname}/study;mkdir {self.inputspath}{self.orchname}/analysis;mkdir {self.inputspath}{self.orchname}/outputs;"
        os.system(cmd)

        cmd = ""
        if self.cluster == "local":
            cmd = f"cp {self.modelspath}mock_preprocessor.py {self.inputspath}{self.orchname}" 
            os.system(cmd)  
            cmd = f"cd {self.inputspath}{self.orchname};python3 mock_preprocessor.py"
        else:
            ## TO BE CHANGED!!!!!!!!!!!!!!!!!!
            ## Get the chmt initial inputs 
            path = f'{os.environ["PLG_GROUPS_STORAGE"]}/plggprimage/vph-hfv3-4.0/initial_input/CHMT/'
            file = ["drugs.is_active.tsv"]
            cmd = f'cp {path}{file[0]} {self.inputspath}{self.orchname}/outputs/'
            try:
                os.system(cmd)
            except:
                print(f"{file[0]} was not found in {path}.")
                sys.exit(1) 

            ## cp analysis.json to orchname folder
            cmd = f'cp {self.inputspath}/analysis.json {self.inputspath}{self.orchname}/study/'
            os.system(cmd)
            cmd = f'singularity run --bind {self.inputspath}{self.orchname}/study/:/study,{self.inputspath}{self.orchname}/outputs/:/outputs,{self.inputspath}{self.orchname}/analysis/:/analysis {os.environ["PLG_GROUPS_STORAGE"]}/plggprimage/preprocessor-mk/preprocessor-start-in-tmp.sif'                   
        try:
            os.system(cmd)
            time.sleep(5)
        except:
            print(f"Preprocessor failed.")
            sys.exit(1) 


    ## Check if all the models exist
    def checkModels(self):
        
        for idmodel in self.models:
            ## check if idmodel written in initial file exist
            query = f"SELECT count(`idmodel`) FROM {self.SQL.mydb.database}.model WHERE `idmodel` = {idmodel}"
            result = self.SQL.mysql_execute(query)[0][0]
            
            ## if idmodel does not exist, finished execution with error
            if result != 1:
                print(f"Model {idmodel} does not exist in the database. Please insert it or choose a valid model id and try again.")
                sys.exit(1)  

        ## Checks if the directory 'Models' exist
        if os.path.isdir(self.modelspath) == False: 
            os.mkdir(self.modelspath)
            sys.exit(f"Directory {self.modelspath} was created. Please, add all the models there and restart the orchestrator.")

        ## Check if directory 'Models' is not empty
        if os.listdir(self.modelspath) == None:
            sys.exit("You need to add all models to the 'Models' folder. Don't forget to add your models on database as well ;)")    
        
        
    ## Check if initial files exist
    def checkii(self):
        
        if self.beginloop == 0: ##If first loop, less initial files
            initialfiles = self.iifiles[0:5]
        else:
            initialfiles = self.iifiles
        print(initialfiles)
            
        ## Checks if there are input files
        iidirfiles = os.listdir(self.inputspath+self.orchname+"/outputs/") 
        for file in initialfiles:
            found = False
            for dirfile in iidirfiles:
                if file == dirfile:
                    found = True
                    break    
            if found == False:
                print(f"Initial file {file} is missing. Please check is the initial inputs were created by the pre-processor script in {self.inputspath}{self.orchname}.")
        
        return initialfiles
                
    
    ## Initialize orchestration
    def orchestration(self):
        
        ## Set orchestration ID
        query = f"SELECT max(idorchestration) FROM {self.SQL.mydb.database}.orchestration"
        nextid = self.SQL.mysql_execute(query)[0][0]
        if nextid == None:
            nextid = 1
        else:
            nextid = nextid + 1

        ## Create directory for the orchestration
        orchpath = self.inputspath+"Orchestration_"+str(nextid)+"/"
        if os.path.isdir(orchpath) == False:
            try:
                os.mkdir(orchpath)
            except:
                print (f"Creation of the directory {str(orchpath)} failed.")
                exit(1)
            else:
                print (f"Successfully created the directory {str(orchpath)}") 


        ## Set the loop to start
        self.beginloop = int(self.initialtime / self.loopsize)
        
        ## Create directory for the loop
        looppath = orchpath+"Loop_"+str(self.beginloop+1)+"/"
        if os.path.isdir(looppath) == False:
            try:
                os.mkdir(looppath)
            except:
                print (f"Creation of the directory {str(looppath)} failed.")
                exit(1)
            else:
                print (f"Successfully created the directory {str(looppath)}") 
                
        return [looppath, nextid]
    
    
    def startSimulation(self):

        ## Creates logfile to simulate cluster
        if self.cluster == 'local':
            outputfile = open("../output.out",'w')
            outputfile.write("Initializing simulation")
            outputfile.close() 

        ## Initialize orchestration
        [looppath, idorch] = self.orchestration()
        print(looppath, idorch)
        
        ## Check initial files exist in the correct folder
        initialfiles = self.checkii()  
        ## Copies initial files to loop path
        for f in initialfiles:
            cmd = f'cp {self.inputspath}{self.orchname}/outputs/{f} {looppath}'
            os.system(cmd)
            
        ## Remove cellularity = 0 on copied file
        if initialfiles != "": ##If preprocessor did not fail
            self.removeSmallCellus(looppath, 0.05)            
            ## Get the number of elements
            nelems = self.getnelems(looppath+self.iifiles[2])
        print(nelems)
        
        ## get initial static data from QUIBIM DB
        values = self.getfromQUIBIM()
        print("QUIBIM data gotten")

        ## Inserts on initial_input table
        columns = "`names_list`, `path`, `datetime`, `histology`, `TERT`, `ATRX`, `MYCN`, `ALT`, `ALK`, `gradiff`, `drug_regime`"
        data = f"\"{self.SQL.listtostring(initialfiles,space=True, comma=True)}\", \"{looppath}\", NOW(), {values[1]}, {values[3]}, {values[4]}, {values[5]}, {values[6]}, {values[7]}, {values[2]}, \"drug_regime\\n{values[0]}\""
        self.SQL.mysql_insert("initial_input", columns, data)  
        
        ## Inserts on orchestration table
        ## define initial and final simulation time
        totalTime = self.calculateTotalTime()
        initialTime = self.beginloop * self.loopsize
        ## Gets new the initial id
        query = "SELECT idinitial_input FROM initial_input ORDER BY idinitial_input DESC LIMIT 1;"
        initialID = self.SQL.mysql_execute(query)[0][0]
        ## inserts an orchestration
        columns = "`idorchestration`, `name`, `total_simtime`, `current_simtime`, `loop_time`, `user`, `status`, `id_initial_input`, `binsrate`, `nGPUs`, `varP`"
        data = f'{str(idorch)}, "{self.orchname}", {str(totalTime)}, {str(initialTime)}, {str(self.loopsize)}, "{os.environ["USER"]}", "Running", {str(initialID)}, {str(self.binsrate)}, {str(self.nGPUs)}, "{self.varP}"'
        self.SQL.mysql_insert("orchestration", columns, data)
        
        ## Set type of simulation
        if self.beginloop == 0:
            desc = "First diffusion"
        else:
            desc = "Continuing previous simulation"
        ## Creates a simulation
        columns = "`id_orchestration`, `description`, `datetime`, `runtime`, `status`, `id_model`, `loop`, `nelems`"
        data = f'{str(idorch)}, \"{desc}\", NOW(), 0, \"running\", {str(self.models[0])}, {self.beginloop}, {str(nelems)}'
        self.SQL.mysql_insert("simulation", columns, data) 
        ## Gets the simulation ID
        query = f"SELECT idsimulation FROM simulation WHERE `id_orchestration` = \"{str(idorch)}\" AND `description` = \"{desc}\""
        simID = self.SQL.mysql_execute(query)[0][0]
        ## Create the subfolder
        subdir = f"{self.names[0]}_{str(simID)}/"
        newPath = looppath+subdir
        ## Creates the directory to I/O storage
        if os.path.isdir(newPath) == False:
            try:
                os.mkdir(newPath)
            except OSError:
                print (f"I/O directory {newPath} exists or the creation failed.")
                sys.exit(1)
            else:
                print ("Successfully created the I/O directory.") 
                    
        ## Define provenance sim
        ## The simulation before the particularization is the provenance sim of a loop
        nbins = int(self.binsrate*nelems)
        ## Set inputs per job
        iperjob = self.inputsperjob(nbins, nelems)

        ## If iperjob will run in more than 3 days, double the number of GPUs and recalculate it
        while iperjob > 12960:## Considering 20 seconds per executions
            self.nGPUs = self.nGPUs*2
            iperjob = self.inputsperjob(nbins, nelems)

        ## Insert into provenance_sim table
        columns = "`idprov`, `inputs_per_job`, `nbins`, `nelems`"
        data = f'{str(simID)}, {iperjob}, {str(nbins)}, {str(nelems)}'
        self.SQL.mysql_insert("provenance_sim", columns, data)
        
        ## Inserts oxygen_tansport input
        columns = "`mesh_elements`, `mesh_nodes`, `vascularization`, `cellularity`, `path`, `datetime`, `provenance_initial_input`, `id_simulation`"
        if initialfiles != "":## If preprocessor did not fail
            data = "\""+self.readfile(self.iifiles[2], looppath)+"\", \""+self.readfile(self.iifiles[3], looppath)+"\", \""+self.readfile(self.iifiles[1], looppath)+"\", \""+self.readfile(self.iifiles[0], looppath)+"\", \""+newPath+"\", NOW(), "+str(initialID)+", "+str(simID)
            self.SQL.mysql_insert("input_"+self.names[0], columns, data)
        else:## Insert nulls
            data = "NULL, NULL, NULL, NULL, "+newPath+"\", NOW(), "+str(initialID)+", "+str(simID)
            self.SQL.mysql_insert("input_"+self.names[0], columns, data)


    ## Set inputs per job    
    def inputsperjob(self, nbins, nelems):

        if nbins > 0:
            iperjob = (nbins+1)/self.nGPUs ## +1 because first bin runs the biggest and the maximum elements
        else: ## nbins = 0 means do not do the homogenization (full-scale case)
            iperjob = nelems/self.nGPUs
        if iperjob < 1:
            iperjob = 1
            self.nGPUs = 1

        return iperjob


    ## Initialize modules
    def inimodules(self):
        
        ## List of modules
        self.ms = [] ## Serializer instances
        self.md = [] ## De-serializer instances
        self.mr = [] ## Relation instances
        for i in range(len(self.names)):
            try:
                self.ms.append(serializer.serializer(self.dbname, self.names[i], self.models[i], self.cluster, self.inputspath, self.modelspath, self.pwd, self.poll_time))
            except:
                print("Serializer module for "+self.names[i]+" model was not initialized.")
                sys.exit(1)
            try:
                self.md.append(deserializer.deserializer(self.dbname, self.names[i], self.models[i], self.cluster, self.inputspath, self.modelspath, self.pwd, self.poll_time, self.nGPUs))
            except:
                print("De-serializer module for "+self.names[i]+" model was not initialized.")
                sys.exit(1)
        for i in range(len(self.relations)):
            if self.relations[i] == "particularization":
                try:
                    self.mr.append(relationO1toC1.O1toC1(self.dbname, self.names[i], self.names[i+1], self.models[i+1], self.cluster, self.inputspath, self.poll_time, self.pwd))
                except:
                    print("Module for "+self.relations[i]+" module was not initialized.")
                    sys.exit(1)
            else:
                if self.relations[i] == "homogenization":
                    try:
                        self.mr.append(relationA1toM1.A1toM1(self.dbname, self.names[i], self.names[i+1], self.names[i-1], self.models[i+1], self.cluster, self.inputspath, self.poll_time, self.pwd))
                    except:
                        print("Module for "+self.relations[i]+" module was not initialized.")
                        sys.exit(1)
                else:
                    if self.relations[i] == "remeshing":
                        try:
                            self.mr.append(relationM1toO1.M1toO1(self.dbname, self.names[i], self.names[0], self.models[0], self.cluster, self.inputspath, self.modelspath, self.poll_time, self.pwd))
                        except:
                            print("Module for "+self.relations[i]+" module was not initialized.")
                            sys.exit(1)
                    else:
                        try:
                            self.mr.append(relationC1toA1.C1toA1(self.dbname, self.names[i],self.names[i+1],self.relations[i],self.models[i+1],self.cluster,self.inputspath,self.poll_time,self.pwd))
                        except:
                            print("Module for "+self.relations[i]+" module was not initialized.")
                            sys.exit(1)

    ## Get the number of elements
    def getnelems(self, path):

        arq = open(path,'r')
        lines = arq.readlines()
        nelems = lines[-1].split(',')[1]

        return int(nelems)

    ## Remove cellularities < minimum
    def removeSmallCellus(self, looppath, minimum):

        ## Save a copy of original cellularity file
        cmd = f"cp {looppath}i_cellularity.txt {looppath}original_i_cellularity.txt"
        os.system(cmd)
        ## Creates new file
        arq = open(looppath+'i_cellularity.txt','w')
        ## Reads original cellularity values
        cellufile = open(looppath+'original_i_cellularity.txt','r')
        for line in cellufile.readlines():
            if line != '':
                cellularity = line.split('\n')[0] ## Read line
                cellularity = cellularity.split() ## Get cellularity value
                if float(cellularity[1]) > minimum:
                    arq.write(f'{cellularity[0]} {cellularity[1]}\n')
                else:
                    arq.write(f"{cellularity[0]} {minimum}\n")
    
    
    ## Returns the content of a file
    def readfile(self, filename, initial_path):

        ## open the file and return the content
        arq = open(initial_path+filename)
        content = arq.readlines()
        content = self.SQL.listtostring(content)
        return content
    

    ## Define final simulation time
    ## It must be a multiple of loop time size
    def calculateTotalTime(self):

        if self.totaltime % self.loopsize != 0:
            return (int(self.totaltime/self.loopsize)+1)*self.loopsize
        else:
            return self.totaltime

    ## Clean exec_table
    def dropexec(self):
        query = f'DELETE FROM {self.SQL.mydb.database}.exec_table;'
        self.SQL.mysql_execute_NOreturn(query)
    
    ## Save results on onedateDB
    def exportresults(self, orchid):

        ## Get results path from final outputs table
        query = f"""select idfinal_output, path, name from {self.SQL.mydb.database}.final_output INNER JOIN {self.SQL.mydb.database}.orchestration ON 
        final_output.idfinal_output = orchestration.id_final_output WHERE idorchestration = {str(orchid)};"""
        results = self.SQL.mysql_execute(query)
        idresult = results[0][0]
        path = results[0][1]
        name = results[0][2]

        ## Run final results script
        if self.cluster == "ares":
            cp =  f"cp {self.modelspath}/getresults.py {path}getresults.py;"
            try:
                os.system(cp)
            except:
                print("getresults.py not found.") 
                sys.exit(1)

            ## run all scripts
            certificate = self.proxy
            backend_conf = "'{\"ENDPOINT\":\"https://submit.plgrid.pl\",\"PROXY\":\""+certificate+"\"}'"
            job_conf = ("'{\"script\":\"#!/bin/bash \\n#SBATCH -J getresults \\n#SBATCH -A plgprimage4-cpu \\n#SBATCH --mem-per-cpu=500MB \\n#SBATCH --time=2:00:00 "
                        "\\n#SBATCH -p plgrid-now \\n#SBATCH --error="+path+"/error.err \\n#SBATCH --output="+path+"/output.out"
                        "\\ncd "+path+" \\nmodule load .plgrid \\nmodule load python/3.8.2-gcccore-9.3.0 \\npip3 install --upgrade pip "
                        "\\npip3 install numpy-stl \\npython3 getresults.py \\necho Done.\", \"host\":\"login01.ares.cyfronet.pl\"}'")

            cmd = f'python3 {os.environ["PLG_GROUPS_STORAGE"]}/plggprimage/Models/hpc-connector/hpc-connector.py --backend Prometheus --backend-conf {backend_conf} job --job-config {job_conf} submit' 
            try:
                os.system(cmd)
            except:
                print(cmd+" FAILED.")
                sys.exit(1)

            ## Wait the job get read
            finished = False
            outputfile = path+"output.out"
            while finished == False:
                if os.path.isfile(outputfile) == True:
                    out = open(outputfile, 'r')
                    line = out.readlines()
                    if line != []:
                        line = line[-1]
                        if line.startswith("Done."):
                            finished = True  

        ## Get id orchestration
        query = f"SELECT idorchestration FROM {self.SQL.mydb.database}.orchestration WHERE id_final_output = {str(idresult)};"
        idorch = self.SQL.mysql_execute(query)[0][0]

        ## Make folder into onedate DB
        finalpath = f'{self.outputspath}{os.environ["USER"]}/{name}/Orchestration_{idorch}/'
        cmd = f'mkdir -p {finalpath}'
        os.system(cmd)
        ## Copy 'path' final to onedate DB
        cmd = f"cp -rf {path}/results.txt {finalpath}/results.txt"
        os.system(cmd)

    
    def getBoolean(dict):
        
        if dict["evaluated"]["yes"] == True:
            if dict["status"]["yes"] == True:
                return 1
            else:
                return 0
        else:
            return -1


    def getBooleanMYCN(dict):
        
        if dict["evaluated"]["yes"] == True:
            if dict["percentage_amplified"]["value"] != None and dict["percentage_amplified"]["value"] != 0:
                return 1
            else:
                return 0
        else:
            return -1

        
    def getBooleanALT(dict):
        
        if dict["amplification"]["evaluated"]["yes"] == True:
            if dict["amplification"]["status"] != None:
                return 1
        else:
            if dict["mutation"]["evaluated"]["yes"] == True:
                if dict["mutation"]["status"] != None:
                    return 1
            else:
                return 0


    def getRegime(self, dict):

        regimes = ""
        if dict["vp_carbo"]["result"]["yes"] == True:
            regimes += 'VP_CARBO\n'
        if dict["cado"]["result"]["yes"] == True:
            regimes += 'CADO\n'
        if dict["co"]["result"]["yes"] == True:
            regimes += 'CO\n'
        if dict["rapid_cojec"]["result"]["yes"] == True:
            regimes += 'RAPID_COJEC\n'
        if dict["modified_n7"]["result"]["yes"] == True:
            regimes += 'MODIFIED_N7\n'
        if dict["n4"]["result"]["yes"] == True:
            regimes += 'N4\n'
        if dict["n5"]["result"]["yes"] == True:
            regimes += 'N5\n'
        if dict["n6"]["result"]["yes"] == True:
            regimes += 'N6\n'
        if dict["n7"]["result"]["yes"] == True:
            regimes += 'N7\n'
        if dict["others"]["result"]["yes"] == True:
            regimes += 'OTHERS\n'
        if regimes == "":
            regimes += 'CADO\n'
        return regimes


    def getGradiff(self, dict):
        
        if dict["value"] == "Undifferentiated":
            return 0
        if dict["value"] == "Poorly differentiated":
            return 1
        if dict["value"] == "Differentiating":
            return 2
        else:
            return -1


    def getHistology(dict):
        
        if dict["value"] == "Neuroblastoma":
            return 0
        if dict["value"] == "Ganglioneuroblastoma":
            return 1
        if dict["value"] == "Nodular ganglioneuroblastoma":
            return 2
        if dict["value"] == "Intermixed ganglioneuroblastoma":
            return 3
        if dict["value"] == "Ganglioneuroma":
            return 4
        if dict["value"] == "Maturing ganglioneuroma":
            return 5
        if dict["value"] == "Mature ganglioneuroma":
            return 6
        else:
            return -1


    ## Connect to QUIBIM DB to get the initial parameters
    def getfromQUIBIM(self):

        ## Connect to Quibim
        # Include the token in the header.
        try:
            if self.quibimtoken != "":
                headers = {'Content-Type': 'application/json', "AuthorizationQuiBim": f"QuiBim {self.quibimtoken}"}
                # Send HTTP GET to retrieve endpoint.
                response = requests.get('https://primage.quibim.com/api/subject/', headers=headers)
                response_as_dict = response.json() # convert JSON to DICT.
                ## Get id
                id = -1
                for item in response_as_dict:
                    if item["code"] == self.orchname:
                        id = item['_id']
                        break

                ## Get data
                params = {"code":"01_Neuroblastoma_45", "project":"Neuroblastoma"}
                response = requests.get(f'https://primage.quibim.com/api/subject/{id}/eform', params=params, headers=headers)
                response_as_dict = response.json() # convert JSON to DICT.
            else:
                headers = {'Content-Type': 'application/json', "AuthorizationQuiBim": f"QuiBim {self.quibimtoken}"}
                params = {"code":"01_Neuroblastoma_45", "project":"Neuroblastoma"}
                response = requests.get("https://primage.quibim.com/api/subject/eform", params=params, headers=headers)
            
                if response["reason"] == "Unauthorized":
                    print("Access not authorized to QUIBIM database. Verify the QUIBIM token.")
                    sys.exit(1)
        except:
            print("Access to QUIBIM API failed.")
            sys.exit(1)

        drugregime = self.getRegime(response_as_dict["content"]["treatment_and_evaluation"]["first_treatment"]["treatment_received"])
        histology = self.getHistology(response_as_dict["content"]["diagnosis"]["laboratory"]["pathology_tumor"]["histology_type"])
        gradiff = self.getGradiff(response_as_dict["content"]["diagnosis"]["laboratory"]["pathology_tumor"]["grade_of_differentiation"])
        tert_rarngm = self.getBoolean(response_as_dict["content"]["diagnosis"]["laboratory"]["molecular_studies"]["tert"])
        atrx_inact = self.getBoolean(response_as_dict["content"]["diagnosis"]["laboratory"]["molecular_studies"]["mutation_atrx"])
        mycn_amp = self.getBooleanMYCN(response_as_dict["content"]["diagnosis"]["laboratory"]["molecular_studies"]["status_mycn"])
        alt = self.getBoolean(response_as_dict["content"]["diagnosis"]["laboratory"]["molecular_studies"]["alt"])
        alk = self.getBooleanALT(response_as_dict["content"]["diagnosis"]["laboratory"]["molecular_studies"]["status_alk"]["mutation"])

        return [drugregime, histology, gradiff, tert_rarngm, atrx_inact, mycn_amp, alt, alk]


    ## Checks if the orchestration's executions finished
    def checkEnd(self):

        ## Update number of initial inputs and final output
        runnings = self.SQL.mysql_execute(f"select idorchestration from {self.SQL.mydb.database}.orchestration WHERE status = \"Running\";")

        for i in range(len(self.ongoingids)):
            running = False
            for idrunning in runnings:
                if self.ongoingids[i][0] == idrunning[0]:
                    running = True
                    break
            ## If orchestration finished, export results to onedate
            if running == False:
                self.exportresults(self.ongoingids[i][0])
                self.ongoingids.pop(i)

        if len(runnings) == 0:
            self.finished = True
            for i in range(len(self.ms)):
                self.ms[i].finished = True
                print(f'Elapsed time during the serializer {self.ms[i].name}: {self.ms[i].total_process}') 
                print(f'Elapsed time during the serializer {self.ms[i].name} polled: {self.ms[i].polling_process}')
            for i in range(len(self.md)):
                self.md[i].finished = True
                print(f'Elapsed time during the de-serializer {self.md[i].name}: {self.md[i].total_process}') 
                print(f'Elapsed time during the de-serializer {self.md[i].name} polled: {self.md[i].polling_process}') 
            for i in range(len(self.mr)):
                self.mr[i].finished = True
                print(f'Elapsed time during the relation {self.mr[i].name}: {self.mr[i].total_process}') 
                print(f'Elapsed time during the relation {self.mr[i].name} polled: {self.mr[i].polling_process}') 
                print(f'Elapsed time during the relation {self.mr[i].name} accessing DB: {self.mr[i].DB_time}')  

            print("Finished!!! \ (^◡^) /")
                
            
if __name__ == "__main__":

    ## SET THE  INITIAL PARAMETERS (MasterCommander parameters) FOLLOWING THIS ORDER
    ## prometheus: boolean to use or not prometheus
    ## password = password DB
    ## dbname = name of the database to be connect or created
    boss = MasterCommander()
    ## Start
    cProfile.run('boss.main()')
