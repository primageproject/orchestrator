
import requests
from requests.auth import HTTPBasicAuth


# Include the token in the header.
headers = {'Content-Type': 'application/json', "AuthorizationQuiBim": "QuiBim eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI2NDViNjQ3YTgzNDJiYTA4M2RjMjZmMWMiLCJpYXQiOjE2ODM3OTA0MzYsImV4cCI6MTY4Mzk2MzIzNn0.m9nJ3Q7uneBxr42pGFasBoNL0zhiasjYM3uJo7pEros"}
params = {"code":"01_Neuroblastoma_45", "project":"Neuroblastoma"}
# Send HTTP GET to retrieve endpoint.
response = requests.get('https://primage.quibim.com/api/subject/eform', params=params, headers=headers)
#response = requests.get('https://primage.quibim.com/api/subject/5f8f5499d562f0200c942e23/eform', params=params, headers=headers)
response_as_dict = response.json() # convert JSON to DICT.

print(response_as_dict)


#response = requests.get("https://primage.quibim.com/api/subject/eform")

#curl -i -k -X GET https://primage.quibim.com/api/subject/eform -H "AuthorizationQuiBim: QuiBim eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI2MjQ0MzA0NDE3NjZlYTA5NWUwNWE5ODkiLCJpYXQiOjE2NDg2MzU5NzUsImV4cCI6MTY0OTkzMTk3NX0.coW7PjbNuIjaSqxfqLaDNTYNw8bY91UaYTXpro2T0kM
#print(response)
