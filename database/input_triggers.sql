DROP TRIGGER IF EXISTS `<db_name>`.`input_<model_name>_AFTER_INSERT`
CREATE DEFINER = CURRENT_USER TRIGGER `<db_name>`.`input_<model_name>_AFTER_INSERT` AFTER INSERT ON `input_<model_name>` FOR EACH ROW
BEGIN
INSERT INTO exec_table 
VALUES(NULL, "input_<model_name>", NOW(), new.idinput_<model_name>,"insert");
END


DROP TRIGGER IF EXISTS `<db_name>`.`input_<model_name>_AFTER_UPDATE` 
CREATE DEFINER = CURRENT_USER TRIGGER `<db_name>`.`input_<model_name>_AFTER_UPDATE` AFTER UPDATE ON `input_<model_name>` FOR EACH ROW
BEGIN
IF new.checksum IS NOT NULL THEN
INSERT INTO exec_table
VALUES(NULL, "input_<model_name>", NOW(), new.idinput_<model_name>,"update");
END IF
END;

