-- -----------------------------------------------------
-- Table `<db_name>`.`orchestration`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `<db_name>`.`orchestration` (
  `idorchestration` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `user` VARCHAR(45) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `runtime` TIME NULL,
  `binsrate` FLOAT NOT NULL,
  `nGPUs` INT NOT NULL,
  `id_initial_input` INT NOT NULL,
  `id_final_output` INT NULL,
  `total_simtime` INT NOT NULL,
  `current_simtime` INT NOT NULL DEFAULT 0,
  `loop_time` INT NOT NULL,
  `varP` VARCHAR(45) NULL,
  PRIMARY KEY (`idorchestration`),
  CONSTRAINT `fk_orchestration_initial_input`
    FOREIGN KEY (`id_initial_input`)
    REFERENCES `<db_name>`.`initial_input` (`idinitial_input`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_orchestration_final_output`
    FOREIGN KEY (`id_final_output`)
    REFERENCES `<db_name>`.`final_output` (`idfinal_output`)
    ON DELETE SET NULL
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

