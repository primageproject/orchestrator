ALTER TABLE `<db_name>`.`input_<model_name>`
ADD COLUMN `provenance_initial_input` INT NULL,
ADD CONSTRAINT `fk_initial_input`
    FOREIGN KEY (`provenance_initial_input`)
    REFERENCES `<db_name>`.`initial_input` (`idinitial_input`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION
