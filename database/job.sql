-- -----------------------------------------------------
-- Table `<db_name>`.`job`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `<db_name>`.`job` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `idjob` INT NULL,
  `id_model` INT NOT NULL,
  `modelfile` VARCHAR(100) NOT NULL,
  `errorfile` VARCHAR(255) NOT NULL,
  `logfile` VARCHAR(255) NOT NULL,
  `status` VARCHAR(20) NOT NULL,
  `idinputs` MEDIUMTEXT NOT NULL,
  `author` VARCHAR(45) NOT NULL,
  `dt_creation` DATETIME NOT NULL,
  `dt_end` DATETIME NULL,
  `queue_time` TIME NULL,
  `execution_time` TIME NULL,
  `runtime` DOUBLE NOT NULL,
  `memory` INT NOT NULL,
  `time` INT NOT NULL,
  `description` VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_model`
    FOREIGN KEY (`id_model`)
    REFERENCES `<db_name>`.`model` (`idmodel`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION
  )
  
ENGINE = InnoDB;