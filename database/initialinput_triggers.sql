DROP TRIGGER IF EXISTS bondInitialInput
CREATE TRIGGER bondInitialInput AFTER INSERT ON input_oxygen_transport
FOR EACH ROW
BEGIN
        IF (NEW.`provenance_initial_input` IS NOT NULL) THEN
                UPDATE initial_input
                SET `id_input_<first>` = NEW.idinput_oxygen_transport WHERE `idinitial_input` = NEW.`provenance_initial_input`;
        END IF;
END