DROP TRIGGER IF EXISTS `<db_name>`.`output_<model_name>_AFTER_INSERT` 
CREATE DEFINER = CURRENT_USER TRIGGER `<db_name>`.`output_<model_name>_AFTER_INSERT` AFTER INSERT ON `output_<model_name>` FOR EACH ROW
BEGIN
INSERT INTO exec_table 
VALUES(NULL, "output_<model_name>", NOW(), new.idoutput_<model_name>,"insert");
END