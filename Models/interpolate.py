# Version: 1.3 2022-11-14
import os
import sys
import shutil
import time
import numpy as np
from numpy import array
from scipy.interpolate import griddata


def centeroidnp(arr):
    length, dim = arr.shape
    return np.array([np.sum(arr[:, i])/length for i in range(dim)])

#We need centroids since we have conc data at gauss points
elem0 = np.genfromtxt('mesh_elements_prev.inp',delimiter =',',usecols=(2,3,4,5))
nodes0 = np.genfromtxt('mesh_nodes_prev.inp',usecols=(2,3,4),delimiter=',')
num_elem0 = len(elem0)

centroid0 = np.zeros((num_elem0,3))
for i in range(0,len(centroid0)):
	array = np.zeros((4,3))
	array[0,:] = nodes0[int(elem0[i,0])-1,0:3]
	array[1,:] = nodes0[int(elem0[i,1])-1,0:3]
	array[2,:] = nodes0[int(elem0[i,2])-1,0:3]
	array[3,:] = nodes0[int(elem0[i,3])-1,0:3]
	centroid0[i,0:3] = centeroidnp(array)

elemf = np.genfromtxt('mesh_elements.inp',delimiter =',',usecols=(2,3,4,5))
nodesf = np.genfromtxt('mesh_nodes.inp',usecols=(2,3,4),delimiter=',')
num_elemf = len(elemf)
num_nodesf = len(nodesf)
centroidf = np.zeros((num_elemf,3))
for i in range(0,len(centroidf)):
	array = np.zeros((4,3))
	array[0,:] = nodesf[int(elemf[i,0])-1,0:3]
	array[1,:] = nodesf[int(elemf[i,1])-1,0:3]
	array[2,:] = nodesf[int(elemf[i,2])-1,0:3]
	array[3,:] = nodesf[int(elemf[i,3])-1,0:3]
	centroidf[i,0:3] = centeroidnp(array)

# =============================================================
## READ DATA
# =============================================================
# updated_cellularity has 7 columns: 
# [1]: alive NB vol fraction, [2]: alive SC vol fraction
# [3]: apoptotic NB vol fraction, [4]: necrotic NB vol fraction, [5]: apoptotic SC vol fraction, [6]: necrotic SC vol fraction
# [7]: total cell count
cellularity0 = np.genfromtxt(r'updated_cellularity.txt')
#min_SC_cellularity = 0.05 #set a minimum fraction of SC cells (adapt rest of cellularities accordingly to sum 1)
#for i in range(len(cellularity0)):
#	if cellularity0[i, 1] < min_SC_cellularity:
#		nsum = cellularity0[i, 0] + cellularity0[i, 2] + cellularity0[i, 3] + cellularity0[i, 4] + cellularity0[i, 5]
#		if nsum > 0.0:
#			cellularity0[i, 0:6] *= ((1 - min_SC_cellularity) / nsum)
#			cellularity0[i, 1] = min_SC_cellularity
#		else:
#			cellularity0[i, 1] = min_SC_cellularity

# vascularization0 = np.genfromtxt(r'ratio_VEGF-NB_SC.txt') # pending to make a law relating this to valcularity
vascularization0 = np.genfromtxt(r'i_vascularization.txt',usecols=(1))
# telomer_length.txt will have 4 columns:
# [1]: mean of the length for NB, [2]: std of the length for NB
# [3]: mean of the length for SC, [4]: std of the length for SC
telomer_length0 = np.genfromtxt(r'telomer_length.txt')
# death_signals.txt will have 8 columns:
# [1]: mean of apoptotic signals for NB, [2]: std of apoptotic signals for NB
# [3]: mean of necrotic signals for NB, [4]: std of necrotic signals for NB
# [5]: mean of apoptotic signals for SC, [6]: std of apoptotic signals for SC
# [7]: mean of necrotic signals for SC, [8]: std of necrotic signals for SC
death_signals0 = np.genfromtxt(r'death_signals.txt')
# differenciation_NB.txt will have 2 columns:
# [1]: mean of the differenciation degree for NB, [2]: std of the differenciation degree for NB
differenciation_NB0 = np.genfromtxt(r'differenciation_NB.txt')

# TODO: USE THE RATIO VEGF-NB_SC TO MODEL VACULARIZATION
# oxygen0 = np.genfromtxt(r'updated_O2.txt') # removed from schema 03/2022

# =============================================================
## PROCESS DATA (INTERPOLATION)
# =============================================================
# extract each of the columns from the cellularity matrix and interpolate one by one
cellularity_alive_NB = griddata(centroid0,cellularity0[:, [0]],centroidf,method = 'nearest')
cellularity_alive_SC = griddata(centroid0,cellularity0[:, [1]],centroidf,method = 'nearest')
cellularity_apop_NB = griddata(centroid0,cellularity0[:, [2]],centroidf,method = 'nearest')
cellularity_necro_NB = griddata(centroid0,cellularity0[:, [3]],centroidf,method = 'nearest')
cellularity_apop_SC = griddata(centroid0,cellularity0[:, [4]],centroidf,method = 'nearest')
cellularity_necro_SC = griddata(centroid0,cellularity0[:, [5]],centroidf,method = 'nearest')
total_cell_count = griddata(centroid0,cellularity0[:, [6]],centroidf,method = 'nearest')
cellularity1 = np.column_stack((cellularity_alive_NB,cellularity_alive_SC,cellularity_apop_NB,cellularity_necro_NB,cellularity_apop_SC,cellularity_necro_SC,total_cell_count))
vascularization1 = griddata(centroid0,vascularization0,centroidf,method = 'nearest')
# extract each of the columns from the telomer length matrix and interpolate one by one
telomer_length_mean_NB = griddata(centroid0,telomer_length0[:, [0]],centroidf,method = 'nearest')
telomer_length_std_NB = griddata(centroid0,telomer_length0[:, [1]],centroidf,method = 'nearest')
telomer_length_mean_SC = griddata(centroid0,telomer_length0[:, [2]],centroidf,method = 'nearest')
telomer_length_std_SC = griddata(centroid0,telomer_length0[:, [3]],centroidf,method = 'nearest')
telomer_length1 = np.column_stack((telomer_length_mean_NB,telomer_length_std_NB,telomer_length_mean_SC,telomer_length_std_SC))
# extract each of the columns from the death signals matrix and interpolate one by one
death_signals_mean_apoptotic_NB = griddata(centroid0,death_signals0[:, [0]],centroidf,method = 'nearest')
death_signals_std_apoptotic_NB = griddata(centroid0,death_signals0[:, [1]],centroidf,method = 'nearest')
death_signals_mean_necrotic_NB = griddata(centroid0,death_signals0[:, [2]],centroidf,method = 'nearest')
death_signals_std_necrotic_NB = griddata(centroid0,death_signals0[:, [3]],centroidf,method = 'nearest')
death_signals_mean_apoptotic_SC = griddata(centroid0,death_signals0[:, [4]],centroidf,method = 'nearest')
death_signals_std_apoptotic_SC = griddata(centroid0,death_signals0[:, [5]],centroidf,method = 'nearest')
death_signals_mean_necrotic_SC = griddata(centroid0,death_signals0[:, [6]],centroidf,method = 'nearest')
death_signals_std_necrotic_SC = griddata(centroid0,death_signals0[:, [7]],centroidf,method = 'nearest')
death_signals1 = np.column_stack((death_signals_mean_apoptotic_NB,death_signals_std_apoptotic_NB,death_signals_mean_necrotic_NB,death_signals_std_necrotic_NB,death_signals_mean_apoptotic_SC,death_signals_std_apoptotic_SC,death_signals_mean_necrotic_SC,death_signals_std_necrotic_SC))
# extract each of the columns from the differentiation degree matrix and interpolate one by one
differentiation_mean_NB = griddata(centroid0,differenciation_NB0[:, [0]],centroidf,method = 'nearest')
differentiation_std_NB = griddata(centroid0,differenciation_NB0[:, [1]],centroidf,method = 'nearest')
differenciation_NB1 =  np.column_stack((differentiation_mean_NB,differentiation_std_NB))
#interpolate oxygen to nodes
# oxygen1 = griddata(centroid0,oxygen0,nodesf,method = 'nearest') # removed from schema 03/2022


# =============================================================
## SAVE DATA
# =============================================================
np.savetxt(r'updated_cellularity.txt',cellularity1)
#np.savetxt(r'i_vascularization.txt',vascularization1)
np.savetxt(r'telomer_length.txt',telomer_length1)
np.savetxt(r'death_signals.txt',death_signals1)
np.savetxt(r'differenciation_NB.txt',differenciation_NB1)

fname="i_vascularization.txt"
with open(fname, 'w') as file:
    for i in range (0,num_elemf):
        file.write('%i  %.3f\n' % (i+1,vascularization1[i]))
        
fname="i_cellularity.txt"
with open(fname, 'w') as file:
    for i in range (0,num_elemf):
        file.write('%i %.3f\n' % (i+1,cellularity1[i,0] + cellularity1[i,1]))