from select import select
import sys
import time
import json
import random
import pandas as pd

class Model:

	def __init__(self, id):

		self.output1 = "log_"+str(id)+".out"
		self.output2 = "error_"+str(id)+".err"
		self.output3 = "jobinfo.txt"
		self.path = '../../../../'


	def main(self, ids):

		## Creates an id list
		idslist = ids.split(" ")
		out1 = open(self.output1, 'w')
		out2 = open(self.output2, 'w')
		out3 = open(self.output3, 'w')

		for i in range(len(idslist)-1):

		## Create output files
			inputname = open("input-"+idslist[i]+".json",'r')
			inputdt = json.load(inputname)
			out4 = "output-"+idslist[i]+".json"
			outputfile = open(out4,'w')

			## Open output to be copied
			output = open("USFD_sample_output.json",'r')
   
			## Get initial values
			o2 = inputdt['config']['environment']['O2']
			vol = inputdt['config']['environment']['V_tumour']
			cellu = inputdt['config']['environment']['cellularity'][0]
			#print(cellu,vol,o2)
   
			## Read IO table
			ios = pd.read_csv("IOs.csv", delimiter=';')
			ios = ios.sort_values(by=['i_cellularity_alive_NB', 'i_volume', 'i_oxygen'])
			df = ios[ios['i_cellularity_alive_NB'] >= cellu ]
			#print(df)
			df = df[df['i_volume'] >= vol]
			#print(df)
			df = df[df['i_oxygen'] >= o2]
			#print(df)
			try:
				selectedout = df.iloc[0]
				## Get data to be writed
				dt = json.load(output)
				dt['primage']['O2'] = selectedout["oxygen"]
				dt['primage']["tumour_volume"] = selectedout["volume"]
				dt['primage']["total_volume_ratio_updated"] = random.uniform(-1,1)#selectedout["volume_ratio"]
				dt['primage']['cellularity'] = [selectedout["cellularity_alive_NB"], selectedout["cellularity_alive_SC"], selectedout["cellularity_apoptotic_NB"], 
									selectedout["cellularity_necrotic_NB"], selectedout["cellularity_apoptotic_SC"], selectedout["cellularity_necrotic_SC"]]
				dt['primage']["nb_telomere_length_sd"] = selectedout["std_telomer_NB"]
				dt['primage']["nb_telomere_length_mean"] = selectedout["mean_telomer_NB"]
				dt['primage']["sc_telomere_length_sd"] = selectedout["std_telomer_SC"]
				dt['primage']["sc_telomere_length_mean"] = selectedout["mean_telomer_SC"]
				dt['primage']["sc_apop_signal_mean"] = selectedout["mean_apoptotic_SC"] 
				dt['primage']["nb_necro_signal_sd"] = selectedout["std_necrotic_NB"]
				dt['primage']["sc_apop_signal_sd"] = selectedout["std_apoptotic_SC"]
				dt['primage']["sc_necro_signal_sd"] = selectedout["std_necrotic_SC"]
				dt['primage']["nb_necro_signal_mean"] = selectedout["mean_necrotic_NB"]
				dt['primage']["nb_apop_signal_sd"] = selectedout["std_apoptotic_NB"]
				dt['primage']["sc_necro_signal_mean"] = selectedout["mean_necrotic_SC"]
				dt['primage']["nb_apop_signal_mean"] = selectedout["mean_apoptotic_NB"]
				dt['primage']["extent_of_differentiation_sd"] = selectedout["std_diff_NB"]
				dt['primage']["extent_of_differentiation_mean"] = selectedout["mean_diff_NB"]
				dt['primage']["ratio_VEGF_NB_SC"] = selectedout["vegf"]
				
			except:
				## Set default
				dt = json.load(output)
				dt['primage']['O2'] = 0.1
				dt['primage']["tumour_volume"] = 1
				dt['primage']["total_volume_ratio_updated"] = random.uniform(-1,1)#selectedout["volume_ratio"]
				dt['primage']['cellularity'] = [0.1, 0.1, 0.1, 0.1, 0.1, 0.1]
				dt['primage']["nb_telomere_length_sd"] = 0.1
				dt['primage']["nb_telomere_length_mean"] = 0.1
				dt['primage']["sc_telomere_length_sd"] = 0.1
				dt['primage']["sc_telomere_length_mean"] = 0.1
				dt['primage']["sc_apop_signal_mean"] = 0.1
				dt['primage']["nb_necro_signal_sd"] = 0.1
				dt['primage']["sc_apop_signal_sd"] = 0.1
				dt['primage']["sc_necro_signal_sd"] = 0.1
				dt['primage']["nb_necro_signal_mean"] = 0.1
				dt['primage']["nb_apop_signal_sd"] = 0.1
				dt['primage']["sc_necro_signal_mean"] = 0.1
				dt['primage']["nb_apop_signal_mean"] = 0.1
				dt['primage']["extent_of_differentiation_sd"] = 0.1
				dt['primage']["extent_of_differentiation_mean"] = 0.1
				dt['primage']["ratio_VEGF_NB_SC"] = 0.1

			json.dump(dt, outputfile)
			
		## Write log info
		jobid = 34567+int(idslist[0])
		out1.write("34567\nWed Feb 24 16:45:04 2021 \n+-----------------------------------------------------------------------------+| NVIDIA-SMI 440.33.01    Driver Version: 440.33.01    CUDA Version: 10.2     |\n|-------------------------------+----------------------+----------------------+\n| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |\n| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |\n|===============================+======================+======================|\n|   0  Tesla K40d          On   | 00000000:81:00.0 Off |                    0 |\n| N/A   39C    P8    15W / 300W |      0MiB / 11441MiB |      0%      Default |\n+-------------------------------+----------------------+----------------------+\n\n+-----------------------------------------------------------------------------+\n| Processes:                                                       GPU Memory |\n|  GPU       PID   Type   Process name                             Usage      |\n|=============================================================================|\n|  No running processes found                                                 |\n+-----------------------------------------------------------------------------+\nGPU 0: Tesla K40d, SM37, Linux, pciBusId 129\n64Bit System Detected\nAllocating Host and Device memory\nNo initial states file specified. Using default values.\nRandom seed: 880567\nGenerated random agent population. NB: 6786, SC: 6786\n"+
		ids)
		time.sleep(3)
		out1.write("\nDone.")
		out2.write(""" plgrid/libs/libpng/1.2.52 loaded.
			plgrid/tools/gcc/6.4.0 loaded.
			plgrid/tools/intel/18.0.0 loaded.
			plgrid/tools/impi/2018 loaded.
			plgrid/apps/ansys/2019R3 loaded.
			srun: Job step aborted: Waiting up to 182 seconds for job step to finish.""")
		#out2.write("""slurmstepd: error: *** JOB 23406045 ON p0597 CANCELLED AT 2022-05-27T17:25:08 DUE TO TIME LIMIT ***
			#slurmstepd: error: *** STEP 23406045.0 ON p0597 CANCELLED AT 2022-05-27T17:25:08 DUE TO TIME LIMIT ***""")
		out3.write("""     State 
			---------- 
			COMPLETED
			COMPLETED 
			""")
		out1.close()
		out2.close()
		out3.close()
	


if __name__ == "__main__":

	m = Model(sys.argv[2])
	m.main(str(sys.argv[1]))

