import sys


class Model:

    def __init__(self, id):

        self.input1 = "growth_APDL.txt"
        self.output1 = "geometry.stl"
        self.output2 = "volume_f.txt"
        self.output3 = f"log_{id}.out"
        self.output4 = f"error_{id}.err"
        self.output5 = "jobinfo.txt"
        self.output6 = "solver_structural.out"
        self.path = '../../../../'


    def main(self):

        ## Create output files
        out1 = open(self.output1, 'w')
        out2 = open(self.output2,'w')
        out3 = open(self.output3, 'w')
        out4 = open(self.output4, 'w')
        out5 = open(self.output5, 'w')
        out6 = open(self.output6, 'w')
     
        ## path to get output to be copied
        iopath = self.path+'vph-hfv3-3.0/Models_examples/mechanical/'

        ## Open output to be copied
        geometry = open(iopath+self.output1,'r')
        
        ## Read input and calculate output
        growth = open(self.input1,'r')
        inigrowth = growth.readlines()
        newvol = []
        for i in range(len(inigrowth)):
            line = inigrowth[i].split('\n')[0]
            line = float(line.split(",")[-1])
            if line > 0:
                newvol.append(str(line)+"\n")
            else:
                newvol.append("0.000\n")

        ## Read data to be copied
        dt_geo = geometry.readlines()

        ## 
        self.printListOnFile(dt_geo, out1)
        self.printListOnFile(newvol, out2)
        out3.write("45678\nDone.")
        out4.write(""" plgrid/libs/libpng/1.2.52 loaded.
            plgrid/tools/gcc/6.4.0 loaded.
            plgrid/tools/intel/18.0.0 loaded.
            plgrid/tools/impi/2018 loaded.
            plgrid/apps/ansys/2019R3 loaded.
            srun: Job step aborted: Waiting up to 182 seconds for job step to finish.""")
        #out2.write("""slurmstepd: error: *** JOB 23406045 ON p0597 CANCELLED AT 2022-05-27T17:25:08 DUE TO TIME LIMIT ***
            #slurmstepd: error: *** STEP 23406045.0 ON p0597 CANCELLED AT 2022-05-27T17:25:08 DUE TO TIME LIMIT ***""")
        out5.write("""     State 
            ---------- 
            COMPLETED
            COMPLETED 
            """)
        out6.write("""/COM,ANSYS RELEASE 2021 R2           BUILD 21.2      UP20210601       15:56:33

            *** WARNING ***                         CP =    1004.784   TIME= 16:42:22
            During this session the elapsed time exceeds the CPU time by 196%.      
            Often this indicates either a lack of physical memory (RAM) required    
            to efficiently handle this simulation or it indicates a particularly    
            slow hard drive configuration.  This simulation can be expected to run  
            faster on identical hardware if additional RAM or a faster hard drive   
            configuration is made available.  For more details, please see the      
            ANSYS Performance Guide which is part of the ANSYS Help system.         """)
        #out6.write("""/COM,ANSYS RELEASE 2021 R2           BUILD 21.2      UP20210601       15:27:42
#
#            *** ERROR ***                           CP =      19.221   TIME= 15:28:18
#            The value of CONC at node 165 is 1113118.62.  It is greater than the    
#            current limit of 1000000 (which can be reset on the NCNV command).      
#            This generally indicates rigid body motion as a result of an            
#            unconstrained model.  Verify that your model is properly constrained.   """)

        out1.close()
        out2.close()
        out3.close()
        out4.close()
        out5.close()
        out6.close()

    ## Converts list to string
    def printListOnFile(self, list, arq):

        for i in range(len(list)):
            arq.write(list[i])

if __name__ == "__main__":

    m = Model(sys.argv[1])
    m.main()
    

