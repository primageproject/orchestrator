import sys
import os


class Model:

    def __init__(self):

        self.file1 = "tumour_mesh.inp"
        self.file2 = "mesh_nodes_prev.inp"
        self.file3 = "mesh_elements_prev.inp"
        self.file4 = "i_cellularity.txt"
        self.file5 = "death_signals.txt"
        self.file6 = "differenciation_NB.txt"
        self.file7 = "telomer_length.txt"
        self.file8 = "i_vascularization.txt"
        self.file9 = "mesh_nodes.inp"
        self.file10 = "mesh_elements.inp"
        self.file11 = "i_cellularity_nodal.txt"
        self.file12 = "i_vascularization_nodal.txt"
        self.file13 = "updated_cellularity.txt"
        self.file14 = "mesh_new.stl"
        self.path = '../../../../'


    def main(self):

        ## Create output files
        file1 = open(self.file1, 'w')
        file2 = open(self.file2, 'w')
        file3 = open(self.file3, 'w')
        file4 = open(self.file4, 'w')
        file5 = open(self.file5, 'w')
        file6 = open(self.file6, 'w')
        file7 = open(self.file7, 'w')
        file8 = open(self.file8, 'w')
        file9 = open(self.file9, 'w')
        file10 = open(self.file10, 'w')
        file11 = open(self.file11, 'w')
        file12 = open(self.file12, 'w')
        file13 = open(self.file13, 'w')
        file14 = open(self.file14, 'w')

        ## path to get output to be copied
        iopath = self.path+'vph-hfv3-3.0/Models_examples/remesh/'

        ## Open output to be copied
        f1 = open(iopath+self.file1,'r')
        f2 = open(iopath+self.file2,'r')
        f3 = open(iopath+self.file3,'r')
        f4 = open(iopath+self.file4,'r')
        f5 = open(iopath+self.file5,'r')
        f6 = open(iopath+self.file6,'r')
        f7 = open(iopath+self.file7,'r')
        f8 = open(iopath+self.file8,'r')
        f9 = open(iopath+self.file9,'r')
        f10 = open(iopath+self.file10,'r')
        f11 = open(iopath+self.file11,'r')
        f12 = open(iopath+self.file12,'r')
        f13 = open(iopath+self.file13,'r')

        ## Read data to be copied
        dt_f1 = f1.readlines()
        dt_f2 = f2.readlines()
        dt_f3 = f3.readlines()
        dt_f4 = f4.readlines()
        dt_f5 = f5.readlines()
        dt_f6 = f6.readlines()
        dt_f7 = f7.readlines()
        dt_f8 = f8.readlines()
        dt_f9 = f9.readlines()
        dt_f10 = f10.readlines()
        dt_f11 = f11.readlines()
        dt_f12 = f12.readlines()
        dt_f13 = f13.readlines()

        ## Write files
        self.printListOnFile(dt_f1, file1)
        self.printListOnFile(dt_f2, file2)
        self.printListOnFile(dt_f3, file3)
        self.printListOnFile(dt_f4, file4)
        self.printListOnFile(dt_f5, file5)
        self.printListOnFile(dt_f6, file6)
        self.printListOnFile(dt_f7, file7)
        self.printListOnFile(dt_f8, file8)
        self.printListOnFile(dt_f9, file9)
        self.printListOnFile(dt_f10, file10)
        self.printListOnFile(dt_f11, file11)
        self.printListOnFile(dt_f12, file12)
        self.printListOnFile(dt_f13, file13)

        file1.close()
        file2.close()
        file3.close()
        file4.close()
        file5.close()
        file6.close()
        file7.close()
        file8.close()
        file9.close()
        file10.close()
        file11.close()
        file12.close()
        file13.close()

        cmd = f"cp {iopath}mesh_new.stl mesh_new.stl"
        os.system(cmd)

    ## Converts list to string
    def printListOnFile(self, list, arq):

        for i in range(len(list)):
            arq.write(list[i])


if __name__ == "__main__":

    m = Model()
    m.main()
    

