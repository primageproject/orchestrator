
import datetime
import json

class Job():
    
    PENDING = 0
    RUNNING = 1
    COMPLETED = 2
    CANCELLED = 3
    UNKNOWN = -1
    FAILED = -2

    __STATE_TO_STR = {  "0": "PENDING", 
                        "1": "RUNNING", 
                        "2": "FINISHED", 
                        "3": "CANCELLED",
                        "-1": "UNKNOWN", 
                        "-2": "FAILED"
                    }

    def __init__(self, _id):
        self._id = _id
        self._state = Job.UNKNOWN
        self._info = {}
        self._config = {}

    def set_config (self, new_config):
        self._config = new_config

    def get_id(self):
        return self._id

    def set_id(self, _id):
        self._id = _id

    def get_configuration(self):
        return self._config
    
    def get_info(self):
        return self._info

    def set_info(self, new_info):
        self._info = { 'time': datetime.datetime.now().isoformat(), 'data': new_info}
    
    def get_state(self):
        return self._state
    
    def set_state(self, new_state):
        if new_state in [Job.COMPLETED, Job.FAILED, Job.PENDING, Job.RUNNING, Job.UNKNOWN, Job.CANCELLED]:
            self._state = new_state
            return True
        else:
            return False
    
    def get_state_str(self):
        return self.__STATE_TO_STR[str(self.get_state())]
    
    def __str__(self):
        msg = "Job ID: " + str(self.get_id())
        msg += "\nState: " + self.get_state_str()
        msg += "\nInformation: " + json.dumps(self.get_info())
        return msg
