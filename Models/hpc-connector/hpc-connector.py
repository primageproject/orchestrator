#! /usr/bin/env python3
import signal
import sys
import time
import argparse
import json
import datetime
import functools
from src import backends_availables 
from src.backends.prometheus import Prometheus
from src.Job import Job

BACKEND = None
JOB = None
MONITORING_PERIOD = 300 # seconds

# Enable flush in print function
print = functools.partial(print, flush=True)

def submit(_job_config):
    global BACKEND, JOB
    try:
        job_config = json.loads(_job_config)
    except:
        print('Error loading --job-config dictionary')
        sys.exit(1)

    # Check input arguments
    job_config = BACKEND.get_job_config(job_config)
    if not job_config:
        print('Something wrong in --job-config dictionary. Please, check the allowed variables using the "help" subcommand')
        sys.exit(1)
    JOB.set_config(job_config)
    print('Submitting job....')
    return BACKEND.job_submit(JOB)

def cancel():
    global BACKEND, JOB
    print('Cancelling %s' % (JOB.get_id()))
    if BACKEND.job_cancel(JOB):
        print('Job successfully cancelled')

def delete():
    print('Deleting %s' % (JOB.get_id()))
    if BACKEND.job_delete(JOB):
        print('Job successfully deleted')

def get_info_by_id():
    print('Getting information of job %s ....' % (JOB.get_id()))
    if not BACKEND.job_get_state(JOB):
        sys.exit(2)
    print(JOB)

def store_file(content, path):
    try:
        f= open(path,"w+")
        f.write(content)
        f.close()
        return True
    except IOError as e:
        print ("I/O error storing the file downloaded ({0}): {1}".format(e.errno, e.strerror))
    except: #handle other exceptions such as attribute errors
        print ("Unexpected error storing the file downloaded:", sys.exc_info()[0])
    return False

def do_monitoring (Namespace):
    global BACKEND, JOB, MONITORING_PERIOD
    if Namespace.monitoring_period != None:
        try:
            MONITORING_PERIOD = int(Namespace.monitoring_period)
        except:
            print("%s cannot be converted to integer. Therefore, MONITORING_PERIOD is %d" %(Namespace.monitoring_period, MONITORING_PERIOD))
            
    print("Monitoring every %d seconds ..." % (MONITORING_PERIOD) )
    while (JOB.get_state() in [Job.PENDING, Job.RUNNING, Job.UNKNOWN]):
        time.sleep(MONITORING_PERIOD)
        msg_conf = "Current"
        if not BACKEND.job_get_state(JOB):
            msg_conf = "Last"
        print("%s - %s job state: %s" % (str(datetime.datetime.now().isoformat()), msg_conf, JOB.get_state_str()) )


def subcommand_job_main(Namespace):
    global BACKEND, JOB
    if Namespace.command_option == 'submit':
        if submit(Namespace.job_config):
            print("Job submmited succesfully")
        print(JOB)
    else:
        # Check if id is defined arguments
        if not Namespace.id:
            print('Error: id is required')
            sys.exit(1)
        JOB.set_id(Namespace.id)

        if Namespace.command_option == 'info':
            get_info_by_id()
        elif Namespace.command_option == 'delete':
            delete()
        elif Namespace.command_option == 'cancel':
            cancel()
        elif Namespace.command_option == 'monitoring':
            do_monitoring(Namespace)
            print("Final job information:\n%s" % (JOB))

def subcommand_file_main(Namespace):
    global BACKEND

    if Namespace.command_option == 'remove':
        if BACKEND.file_remove(Namespace.src):
            print("File removed succesfully")
    elif Namespace.command_option == 'download':
        file_content = BACKEND.file_download(Namespace.src)
        if file_content: 
            print("File dowloaded succesfully")
            if Namespace.dest:
                store_file(file_content, Namespace.dest)
            else:
                print( 'Content:\n%s'% (file_content) )
    elif Namespace.command_option == 'upload':
        # Check if dest is defined 
        if not Namespace.dest:
            print('Error: dest is required to execute "%s" ' %(Namespace.command_option))
            sys.exit(1)
        if BACKEND.file_upload(Namespace.src, Namespace.dest) :
            print("File uploaded succesfully") 
            
def subcommand_directory_main(Namespace):
    global BACKEND
    if (eval("BACKEND.directory_"+Namespace.command_option )(Namespace.path) ):
            print("Directory "+Namespace.command_option+"d succesfully") 


def subcommand_simulate_main(Namespace):
    global BACKEND, JOB, MONITORING_PERIOD

    if submit(Namespace.job_config):
        print("Job submmited succesfully: \n%s" % (JOB))

        # Capturing SIGINT signal
        signal.signal(signal.SIGINT, stop)

        do_monitoring(Namespace)
                
        print("Final job information:\n%s" % (JOB))

        if Namespace.print_logs:            
            final_log_content = BACKEND.job_get_logs(JOB, Namespace.logs_config)
            if final_log_content:
                print(final_log_content)
            else: 
                print('Something wrong getting logs')
        

def stop(signal, frame):
    """
    Callback function to catch the system signals
    """
    global BACKEND, JOB
    print('Signal SIGINT caught by the hpc-connector --> Stopping the job in the backend')
    cancel()

def create_parser():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(title='Subcommands', help= 'Valid subcommands', dest='command')

    parser.add_argument('--backend', help='Select the backend to run the job', choices=backends_availables, required=True )
    parser.add_argument('--backend-conf', help='Dictionary with specific backend configuration. Use the help subcommand to see the required variables. Example: "{}"', required=True )
    
    job_parser = subparsers.add_parser('job', help='Executes the option selected iteracting with the backend')
    job_parser.add_argument('command_option', help='Option to execute', choices=['submit', 'delete', 'cancel', 'info', 'monitoring'])
    job_parser.add_argument('--id', help='Job name or ID' )
    job_parser.add_argument('--job-config', help='Dictionary with specific job configuration for the selected backend. Use the help subcommand to see the required variables' )
    job_parser.add_argument('--monitoring-period', help='Seconds between getting information about the job state in the backend. Default: 300 seconds' )
    
    simulate_parser = subparsers.add_parser('simulate', help='Submits a job in the selected backend and waits until it ends')
    simulate_parser.add_argument('--job-config', help='Dictionary with specific job configuration for the selected backend. Use the help subcommand to see the required variables' )
    simulate_parser.add_argument('--monitoring-period', help='Seconds between getting information about the job state in the backend. Default: 300 seconds' )
    simulate_parser.add_argument('--print-logs', help='Print logs at the end of the execution. If additional configuration is required of your backend for gatter the logs, use also the argument "--logs-config"', action='store_true', required=False )
    simulate_parser.add_argument('--logs-config', help='Dictionary with specific logs configuration for the selected backend. Use the help subcommand to see the required variables', required=False)

    file_parser = subparsers.add_parser('file', help='Executes the option selected iteracting with the backend')
    file_parser.add_argument('command_option', help='Option to execute', choices=['upload', 'download', 'remove'])
    file_parser.add_argument('src', help='It is the file that will be uploaded to the backend or the URI at the backend that will be dowloaded or removed')
    file_parser.add_argument('--dest', help='Destination URI in the backend. If it is used for the command "download", the file will be stored in that path.', required=False)
    
    directory_parser = subparsers.add_parser('directory', help='Executes the option selected iteracting with the backend')
    directory_parser.add_argument('command_option', help='Option to execute', choices=['list', 'create', 'remove'])
    directory_parser.add_argument('path', help='Directory URI in the backend to be listed, created o removed')

    help_parser = subparsers.add_parser('help', help='Executes the help of selected backend')

    return parser
    
        
if __name__ == "__main__":

    parser = create_parser()
    Namespace = parser.parse_args(sys.argv[1:])

    if Namespace.backend == 'Prometheus':
        BACKEND = Prometheus('Prometheus')
        
    if Namespace.command in [None, 'help']:
        print('Showing BACKEND %s help: '%(Namespace.backend))
        BACKEND.help()
        sys.exit(0)
    
    backend_conf = {}
    try:
        backend_conf = json.loads(Namespace.backend_conf)
    except:
        print('Error loading backend_conf dictonary')

    if not BACKEND.configure_backend( backend_conf ):
        BACKEND.help()
        sys.exit(1)  

    JOB = Job(None)

    if Namespace.command == 'job':
        subcommand_job_main(Namespace)
    elif Namespace.command == 'simulate':
        subcommand_simulate_main(Namespace)
    elif Namespace.command == 'file':
        subcommand_file_main(Namespace)
    elif Namespace.command == 'directory':
        subcommand_directory_main(Namespace)