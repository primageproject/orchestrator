import os
import numpy as np
from stl import mesh
# Get the current working directory
cwd = os.getcwd()

# Define the file names
input_file_first_loop = os.path.join(cwd, "../../Loop_1/remeshing/geometry.stl")
input_file_last_loop = os.path.join(cwd, "mesh_new.stl")
output_file = os.path.join(cwd, "results.txt")

# Read the contents of the input file
stl_mesh_first_loop = mesh.Mesh.from_file(input_file_first_loop)
volume_first_loop, cog, inertia = stl_mesh_first_loop.get_mass_properties()

stl_mesh_last_loop = mesh.Mesh.from_file(input_file_last_loop)
volume_last_loop, cog, inertia = stl_mesh_last_loop.get_mass_properties()

# Write the contents to the output file
with open(output_file, "w") as f:
    f.write("{:.2f}".format(np.abs(volume_first_loop)))
    f.write("\n")
    f.write("{:.2f}".format(np.abs(volume_last_loop)))
    
    
