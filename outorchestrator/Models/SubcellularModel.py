import pandas as pd
import numpy as np
import sys

Niter=int(sys.argv[1])

#Load files
drug_regime=pd.read_csv('drug_regime.csv').values.tolist()
df_vascular = pd.read_csv('vascularization.txt', sep=" ", header=None, names=["ID", "Ktrans"])
Ktrans=df_vascular['Ktrans'].to_numpy().reshape(-1,1)
drug_active=pd.read_csv('drugs.is_active.tsv', delimiter='\t')

min_chemKtrans=0.000136 #0.0142 ± 0.006 min(-1) y Ktrans s(-1)
max_chemKtrans=0.000283 #0.017 min(-1)
p_effects=(Ktrans-min_chemKtrans)/(max_chemKtrans-min_chemKtrans)
p_effects[np.where(p_effects<0)[0]]=0
p_effects[np.where(p_effects>1)[0]]=1

#Drug regime for each treatment
ch_name=['VP/Carbo','CADO','CO','Rapid COJEC','Modified N7','N4','N5','N6','N7','TVD','VCR','CFM','TEMIRI','ICE','CE']
drug_name=[['etoposide', 'carboplatin'],
           ['cyclophosphamide', 'doxorubicin', 'vincristine'],
           ['cisplatin','vincristine'],['cisplatin', 'vincristine', 'carboplatin', 'etoposide', 'cyclophosphamide'],
           ['cisplatin', 'vincristine', 'etoposide', 'cyclophosphamide', 'doxorubicin'],
           ['cyclophosphamide', 'doxorubicin'],
           ['cisplatin', 'etoposide', 'vindesine'],
           ['vincristine', 'dacarbazine', 'isofosfamide', 'doxorubicin'],
           ['cisplatin', 'vincristine', 'etoposide', 'cyclophosphamide', 'doxorubicin'],
           ['topotecan', 'vincristine', 'doxorubicin'],
           ['vincristine'],
           ['cyclophosphamide'],
           ['temozolomide', 'irinotecan'],
           ['isofosfamide', 'carboplatin', 'etoposide'],
           ['carboplatin', 'etoposide']]
drug_time=np.array([[2,3,21,2],
          [2,5,21,4],
          [2,1,21,4],
          [2,3,10,8],
          [2,3,21,5],
          [2,3,21,4],
          [2,3,21,3],
          [2,3,21,3],
          [2,3,21,4],
          [2,3,21,4],
          [2,3,21,2],
          [2,3,21,4],
          [2,3,21,2],
          [2,3,21,1]])#Terminal Half-Life,Duration Cycle,Rest between Cycles,Number Cycles

l_dr=[]
for i in range(len(drug_regime)):
    l_dr+=drug_name[ch_name.index(drug_regime[i][0])]
#Remove duplicates
l_dr = list(set(l_dr))

#Calculate the effects of the drug regime
l_drugID=drug_active['drug'].to_list()
l_ID=[]
for i in range(len(l_dr)):
    for j in range(len(l_drugID)):
        if l_drugID[j].startswith(l_dr[i]):
            l_ID.append(j)

is_active=drug_active.loc[l_ID].max()[1:].to_numpy(dtype=int).reshape(1,-1)
drug_effects=p_effects*is_active

#Effects duration
Tstep=14 #Timestep for each iteration (2 weeks)
dt=drug_time[ch_name.index(drug_regime[0][0])]
start_point=[]
start_iter=[]
end_point=[]
end_iter=[]
for c in range(dt[3]):
    start_point.append(dt[2]*c%Tstep)
    start_iter.append(int(np.floor(dt[2]*c/Tstep)))
    end_point.append((dt[2]*c+(dt[0]+dt[1]-1))%Tstep)
    end_iter.append(int(np.floor((dt[2]*c+(dt[0]+dt[1]-1))/Tstep)))
start_point=np.array(start_point)
start_iter=np.array(start_iter)
end_point=np.array(end_point)
end_iter=np.array(end_iter)

#Effects duration in this iteration
start_effects=start_point[np.where(start_iter==Niter)[0]]
end_effects=end_point[np.where(end_iter==Niter)[0]]
if len(start_effects)>len(end_effects): #The effects doesn't end in this iteration
    end_effects=np.insert(end_effects, len(end_effects), Tstep)
elif len(end_effects)>len(start_effects): #There are effects from the previous iteration
    if end_effects[0]==0: #These effects ends at the begging of the iteration
        end_effects = np.delete(end_effects,0)
    else:
        start_effects=np.insert(start_effects, 0, 0)

start_effects=start_effects*24 #From days to hours
end_effects=end_effects*24 #From days to hours

#Convert to DataFrame
drug_effects=pd.DataFrame(drug_effects,columns=drug_active.columns[1:])
drug_effects.rename(columns={'COPS5': 'JAB1'},inplace=True)
start_effects=pd.DataFrame(start_effects,columns=['start_effects'])
end_effects=pd.DataFrame(end_effects,columns=['end_effects'])

#Save Files
drug_effects.to_csv('drug_effects.csv', index=False)
start_effects.to_csv('start_effects.csv', index=False)
end_effects.to_csv('end_effects.csv', index=False)
